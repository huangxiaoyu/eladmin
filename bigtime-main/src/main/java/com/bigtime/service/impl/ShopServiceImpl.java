/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.impl;

import com.bigtime.domain.Shop;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.bigtime.repository.ShopRepository;
import com.bigtime.service.ShopService;
import com.bigtime.service.dto.ShopDto;
import com.bigtime.service.dto.ShopQueryCriteria;
import com.bigtime.service.mapstruct.ShopMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author huangxiaoyu
* @date 2021-04-29
**/
@Service
@RequiredArgsConstructor
public class ShopServiceImpl implements ShopService {

    private final ShopRepository shopRepository;
    private final ShopMapper shopMapper;

    @Override
    public Map<String,Object> queryAll(ShopQueryCriteria criteria, Pageable pageable){
        Page<Shop> page = shopRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(shopMapper::toDto));
    }

    @Override
    public List<ShopDto> queryAll(ShopQueryCriteria criteria){
        return shopMapper.toDto(shopRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public ShopDto findById(Long id) {
        Shop shop = shopRepository.findById(id).orElseGet(Shop::new);
        ValidationUtil.isNull(shop.getId(),"Shop","id",id);
        return shopMapper.toDto(shop);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ShopDto create(Shop resources) {
        return shopMapper.toDto(shopRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Shop resources) {
        Shop shop = shopRepository.findById(resources.getId()).orElseGet(Shop::new);
        ValidationUtil.isNull( shop.getId(),"Shop","id",resources.getId());
        shop.copy(resources);
        shopRepository.save(shop);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            shopRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<ShopDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ShopDto shop : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("名称", shop.getName());
            map.put("桌子数量", shop.getTable());
            map.put("负责人", shop.getUserId());
            map.put("电话", shop.getPhone());
            map.put("地址", shop.getAddress());
            map.put("状态", shop.getStatus());
            map.put("创建时间", shop.getCreateDate());
            map.put("是否可用", shop.getEnabled());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}