/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.rest;

import me.zhengjie.annotation.Log;
import com.bigtime.domain.Staff;
import com.bigtime.service.StaffService;
import com.bigtime.service.dto.StaffQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author huangxiaoyu
* @date 2021-04-29
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "业务:员工管理")
@RequestMapping("/api/staff")
public class StaffController {

    private final StaffService staffService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('staff:list')")
    public void download(HttpServletResponse response, StaffQueryCriteria criteria) throws IOException {
        staffService.download(staffService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询staff")
    @ApiOperation("查询staff")
    @PreAuthorize("@el.check('staff:list')")
    public ResponseEntity<Object> query(StaffQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(staffService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增staff")
    @ApiOperation("新增staff")
    @PreAuthorize("@el.check('staff:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody Staff resources){
        return new ResponseEntity<>(staffService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改staff")
    @ApiOperation("修改staff")
    @PreAuthorize("@el.check('staff:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody Staff resources){
        staffService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除staff")
    @ApiOperation("删除staff")
    @PreAuthorize("@el.check('staff:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        staffService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}