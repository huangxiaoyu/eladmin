/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.impl;

import com.bigtime.domain.RechargeRecord;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.bigtime.repository.RechargeRecordRepository;
import com.bigtime.service.RechargeRecordService;
import com.bigtime.service.dto.RechargeRecordDto;
import com.bigtime.service.dto.RechargeRecordQueryCriteria;
import com.bigtime.service.mapstruct.RechargeRecordMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author huangxiaoyu
* @date 2021-04-29
**/
@Service
@RequiredArgsConstructor
public class RechargeRecordServiceImpl implements RechargeRecordService {

    private final RechargeRecordRepository rechargeRecordRepository;
    private final RechargeRecordMapper rechargeRecordMapper;

    @Override
    public Map<String,Object> queryAll(RechargeRecordQueryCriteria criteria, Pageable pageable){
        Page<RechargeRecord> page = rechargeRecordRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(rechargeRecordMapper::toDto));
    }

    @Override
    public List<RechargeRecordDto> queryAll(RechargeRecordQueryCriteria criteria){
        return rechargeRecordMapper.toDto(rechargeRecordRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public RechargeRecordDto findById(Long id) {
        RechargeRecord rechargeRecord = rechargeRecordRepository.findById(id).orElseGet(RechargeRecord::new);
        ValidationUtil.isNull(rechargeRecord.getId(),"RechargeRecord","id",id);
        return rechargeRecordMapper.toDto(rechargeRecord);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public RechargeRecordDto create(RechargeRecord resources) {
        return rechargeRecordMapper.toDto(rechargeRecordRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(RechargeRecord resources) {
        RechargeRecord rechargeRecord = rechargeRecordRepository.findById(resources.getId()).orElseGet(RechargeRecord::new);
        ValidationUtil.isNull( rechargeRecord.getId(),"RechargeRecord","id",resources.getId());
        rechargeRecord.copy(resources);
        rechargeRecordRepository.save(rechargeRecord);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            rechargeRecordRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<RechargeRecordDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (RechargeRecordDto rechargeRecord : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("套餐id", rechargeRecord.getComboId());
            map.put("会员id", rechargeRecord.getMemberId());
            map.put("操作员id", rechargeRecord.getUserId());
            map.put("店铺id", rechargeRecord.getShopId());
            map.put("提成", rechargeRecord.getPushMoney());
            map.put("是否启用", rechargeRecord.getEnabled());
            map.put("创建时间", rechargeRecord.getCreateDate());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}