ALTER TABLE `bigtime`.`sys_dept`
ADD COLUMN `user_id` bigint NULL COMMENT '部门领导' AFTER `pid`;


INSERT INTO `bigtime`.`sys_role` (`role_id`, `name`, `level`, `description`, `data_scope`, `create_by`, `update_by`, `create_time`, `update_time`) VALUES (3, '老板', 2, NULL, '本级', 'admin', 'admin', '2021-03-06 14:28:49', '2021-03-06 14:28:49');
INSERT INTO `bigtime`.`sys_role` (`role_id`, `name`, `level`, `description`, `data_scope`, `create_by`, `update_by`, `create_time`, `update_time`) VALUES (4, '经理', 3, NULL, '本级', 'admin', 'admin', '2021-03-06 14:29:11', '2021-03-06 14:29:11');
UPDATE `bigtime`.`sys_role` SET `name` = '普通用户', `level` = 4, `description` = '-', `data_scope` = '本级', `create_by` = NULL, `update_by` = 'admin', `create_time` = '2018-11-23 13:09:06', `update_time` = '2021-03-06 16:42:00' WHERE `role_id` = 2;

