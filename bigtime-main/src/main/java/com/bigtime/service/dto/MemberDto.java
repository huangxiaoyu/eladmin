/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author huangxiaoyu
* @date 2021-04-29
**/
@Data
public class MemberDto implements Serializable {

    private Long id;

    /** 儿童姓名 */
    private String name;

    /** 性别 */
    private String gender;

    /** 年龄 */
    private Integer age;

    /** 父母姓名 */
    private String parentsName;

    /** 练习方式 */
    private String phone;

    /** 第二联系人姓名 */
    private String otherName;

    /** 第二联系人电话 */
    private String otherPhone;

    /** 创建者id */
    private Long createId;

    /** 店铺名称 */
    private Long shopId;

    /** 用时 */
    private Integer useTime;

    /** 总时长 */
    private Integer totalTime;

    /** 购买总时长 */
    private Integer buyTime;

    /** 免费总时长 */
    private Integer freeTime;

    /** 状态 */
    private Integer status;

    /** 是否启用 */
    private Integer enabled;

    /** 谁修改数据 */
    private String updateBy;

    /** 修改时间 */
    private Timestamp updateTime;

    /** 创建时间 */
    private Timestamp createTime;
}