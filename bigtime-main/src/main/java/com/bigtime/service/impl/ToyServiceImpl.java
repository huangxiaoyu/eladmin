/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.impl;

import com.bigtime.domain.Toy;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.bigtime.repository.ToyRepository;
import com.bigtime.service.ToyService;
import com.bigtime.service.dto.ToyDto;
import com.bigtime.service.dto.ToyQueryCriteria;
import com.bigtime.service.mapstruct.ToyMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author huangxiaoyu
* @date 2021-04-29
**/
@Service
@RequiredArgsConstructor
public class ToyServiceImpl implements ToyService {

    private final ToyRepository toyRepository;
    private final ToyMapper toyMapper;

    @Override
    public Map<String,Object> queryAll(ToyQueryCriteria criteria, Pageable pageable){
        Page<Toy> page = toyRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(toyMapper::toDto));
    }

    @Override
    public List<ToyDto> queryAll(ToyQueryCriteria criteria){
        return toyMapper.toDto(toyRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public ToyDto findById(Long id) {
        Toy toy = toyRepository.findById(id).orElseGet(Toy::new);
        ValidationUtil.isNull(toy.getId(),"Toy","id",id);
        return toyMapper.toDto(toy);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ToyDto create(Toy resources) {
        return toyMapper.toDto(toyRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Toy resources) {
        Toy toy = toyRepository.findById(resources.getId()).orElseGet(Toy::new);
        ValidationUtil.isNull( toy.getId(),"Toy","id",resources.getId());
        toy.copy(resources);
        toyRepository.save(toy);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            toyRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<ToyDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ToyDto toy : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("玩具编号", toy.getToyNo());
            map.put("姓名", toy.getName());
            map.put("价格", toy.getPrice());
            map.put("颗粒数", toy.getGrain());
            map.put("适合年龄段", toy.getAgeGroup());
            map.put("难度等级", toy.getLevel());
            map.put("是否用", toy.getEnabled());
            map.put("创建时间", toy.getCreateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}