/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.rest;

import me.zhengjie.annotation.Log;
import com.bigtime.domain.Toy;
import com.bigtime.service.ToyService;
import com.bigtime.service.dto.ToyQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author huangxiaoyu
* @date 2021-04-29
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "业务:玩具管理")
@RequestMapping("/api/toy")
public class ToyController {

    private final ToyService toyService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('toy:list')")
    public void download(HttpServletResponse response, ToyQueryCriteria criteria) throws IOException {
        toyService.download(toyService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询toy")
    @ApiOperation("查询toy")
    @PreAuthorize("@el.check('toy:list')")
    public ResponseEntity<Object> query(ToyQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(toyService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增toy")
    @ApiOperation("新增toy")
    @PreAuthorize("@el.check('toy:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody Toy resources){
        return new ResponseEntity<>(toyService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改toy")
    @ApiOperation("修改toy")
    @PreAuthorize("@el.check('toy:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody Toy resources){
        toyService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除toy")
    @ApiOperation("删除toy")
    @PreAuthorize("@el.check('toy:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        toyService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}