/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author huangxiaoyu
* @date 2021-04-29
**/
@Entity
@Data
@Table(name="big_time_toy")
public class Toy implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "id")
    private Long id;

    @Column(name = "toy_no")
    @ApiModelProperty(value = "玩具编号")
    private String toyNo;

    @Column(name = "name")
    @ApiModelProperty(value = "姓名")
    private String name;

    @Column(name = "price")
    @ApiModelProperty(value = "价格")
    private BigDecimal price;

    @Column(name = "grain")
    @ApiModelProperty(value = "颗粒数")
    private Integer grain;

    @Column(name = "age_group")
    @ApiModelProperty(value = "适合年龄段")
    private String ageGroup;

    @Column(name = "level")
    @ApiModelProperty(value = "难度等级")
    private Integer level;

    @Column(name = "enabled")
    @ApiModelProperty(value = "是否用")
    private Integer enabled;

    @Column(name = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Timestamp createTime;

    public void copy(Toy source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}