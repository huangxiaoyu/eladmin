/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.rest;

import me.zhengjie.annotation.Log;
import com.bigtime.domain.PlayerCashlogDetail;
import com.bigtime.service.PlayerCashlogDetailService;
import com.bigtime.service.dto.PlayerCashlogDetailQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author huangxiaoyu
* @date 2021-04-29
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "业务:消费明细")
@RequestMapping("/api/playerCashlogDetail")
public class PlayerCashlogDetailController {

    private final PlayerCashlogDetailService playerCashlogDetailService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('playerCashlogDetail:list')")
    public void download(HttpServletResponse response, PlayerCashlogDetailQueryCriteria criteria) throws IOException {
        playerCashlogDetailService.download(playerCashlogDetailService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询playercashlogdetail")
    @ApiOperation("查询playercashlogdetail")
    @PreAuthorize("@el.check('playerCashlogDetail:list')")
    public ResponseEntity<Object> query(PlayerCashlogDetailQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(playerCashlogDetailService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增playercashlogdetail")
    @ApiOperation("新增playercashlogdetail")
    @PreAuthorize("@el.check('playerCashlogDetail:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PlayerCashlogDetail resources){
        return new ResponseEntity<>(playerCashlogDetailService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改playercashlogdetail")
    @ApiOperation("修改playercashlogdetail")
    @PreAuthorize("@el.check('playerCashlogDetail:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PlayerCashlogDetail resources){
        playerCashlogDetailService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除playercashlogdetail")
    @ApiOperation("删除playercashlogdetail")
    @PreAuthorize("@el.check('playerCashlogDetail:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        playerCashlogDetailService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}