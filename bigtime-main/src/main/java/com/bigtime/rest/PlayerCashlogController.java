/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.rest;

import me.zhengjie.annotation.Log;
import com.bigtime.domain.PlayerCashlog;
import com.bigtime.service.PlayerCashlogService;
import com.bigtime.service.dto.PlayerCashlogQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author huangxiaoyu
* @date 2021-04-29
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "业务:消费记录")
@RequestMapping("/api/playerCashlog")
public class PlayerCashlogController {

    private final PlayerCashlogService playerCashlogService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('playerCashlog:list')")
    public void download(HttpServletResponse response, PlayerCashlogQueryCriteria criteria) throws IOException {
        playerCashlogService.download(playerCashlogService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询playercashlog")
    @ApiOperation("查询playercashlog")
    @PreAuthorize("@el.check('playerCashlog:list')")
    public ResponseEntity<Object> query(PlayerCashlogQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(playerCashlogService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增playercashlog")
    @ApiOperation("新增playercashlog")
    @PreAuthorize("@el.check('playerCashlog:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody PlayerCashlog resources){
        return new ResponseEntity<>(playerCashlogService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改playercashlog")
    @ApiOperation("修改playercashlog")
    @PreAuthorize("@el.check('playerCashlog:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody PlayerCashlog resources){
        playerCashlogService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除playercashlog")
    @ApiOperation("删除playercashlog")
    @PreAuthorize("@el.check('playerCashlog:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        playerCashlogService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}