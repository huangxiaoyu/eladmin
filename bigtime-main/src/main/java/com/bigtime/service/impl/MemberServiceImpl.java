/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.impl;

import com.bigtime.domain.Member;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.bigtime.repository.MemberRepository;
import com.bigtime.service.MemberService;
import com.bigtime.service.dto.MemberDto;
import com.bigtime.service.dto.MemberQueryCriteria;
import com.bigtime.service.mapstruct.MemberMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author huangxiaoyu
* @date 2021-04-29
**/
@Service
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService {

    private final MemberRepository memberRepository;
    private final MemberMapper memberMapper;

    @Override
    public Map<String,Object> queryAll(MemberQueryCriteria criteria, Pageable pageable){
        Page<Member> page = memberRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(memberMapper::toDto));
    }

    @Override
    public List<MemberDto> queryAll(MemberQueryCriteria criteria){
        return memberMapper.toDto(memberRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public MemberDto findById(Long id) {
        Member member = memberRepository.findById(id).orElseGet(Member::new);
        ValidationUtil.isNull(member.getId(),"Member","id",id);
        return memberMapper.toDto(member);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public MemberDto create(Member resources) {
        return memberMapper.toDto(memberRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Member resources) {
        Member member = memberRepository.findById(resources.getId()).orElseGet(Member::new);
        ValidationUtil.isNull( member.getId(),"Member","id",resources.getId());
        member.copy(resources);
        memberRepository.save(member);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            memberRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<MemberDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (MemberDto member : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("儿童姓名", member.getName());
            map.put("性别", member.getGender());
            map.put("年龄", member.getAge());
            map.put("父母姓名", member.getParentsName());
            map.put("练习方式", member.getPhone());
            map.put("第二联系人姓名", member.getOtherName());
            map.put("第二联系人电话", member.getOtherPhone());
            map.put("创建者id", member.getCreateId());
            map.put("店铺名称", member.getShopId());
            map.put("用时", member.getUseTime());
            map.put("总时长", member.getTotalTime());
            map.put("购买总时长", member.getBuyTime());
            map.put("免费总时长", member.getFreeTime());
            map.put("状态", member.getStatus());
            map.put("是否启用", member.getEnabled());
            map.put("谁修改数据", member.getUpdateBy());
            map.put("修改时间", member.getUpdateTime());
            map.put("创建时间", member.getCreateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}