/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author huangxiaoyu
* @date 2021-04-29
**/
@Data
public class RechargeRecordDto implements Serializable {

    private Long id;

    /** 套餐id */
    private Long comboId;

    /** 会员id */
    private Long memberId;

    /** 操作员id */
    private Long userId;

    /** 店铺id */
    private Long shopId;

    /** 提成 */
    private String pushMoney;

    /** 是否启用 */
    private Integer enabled;

    /** 创建时间 */
    private Timestamp createDate;
}