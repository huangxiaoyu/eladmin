/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.impl;

import com.bigtime.domain.PlayerCashlog;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.bigtime.repository.PlayerCashlogRepository;
import com.bigtime.service.PlayerCashlogService;
import com.bigtime.service.dto.PlayerCashlogDto;
import com.bigtime.service.dto.PlayerCashlogQueryCriteria;
import com.bigtime.service.mapstruct.PlayerCashlogMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author huangxiaoyu
* @date 2021-04-29
**/
@Service
@RequiredArgsConstructor
public class PlayerCashlogServiceImpl implements PlayerCashlogService {

    private final PlayerCashlogRepository playerCashlogRepository;
    private final PlayerCashlogMapper playerCashlogMapper;

    @Override
    public Map<String,Object> queryAll(PlayerCashlogQueryCriteria criteria, Pageable pageable){
        Page<PlayerCashlog> page = playerCashlogRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(playerCashlogMapper::toDto));
    }

    @Override
    public List<PlayerCashlogDto> queryAll(PlayerCashlogQueryCriteria criteria){
        return playerCashlogMapper.toDto(playerCashlogRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public PlayerCashlogDto findById(Long id) {
        PlayerCashlog playerCashlog = playerCashlogRepository.findById(id).orElseGet(PlayerCashlog::new);
        ValidationUtil.isNull(playerCashlog.getId(),"PlayerCashlog","id",id);
        return playerCashlogMapper.toDto(playerCashlog);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PlayerCashlogDto create(PlayerCashlog resources) {
        return playerCashlogMapper.toDto(playerCashlogRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PlayerCashlog resources) {
        PlayerCashlog playerCashlog = playerCashlogRepository.findById(resources.getId()).orElseGet(PlayerCashlog::new);
        ValidationUtil.isNull( playerCashlog.getId(),"PlayerCashlog","id",resources.getId());
        playerCashlog.copy(resources);
        playerCashlogRepository.save(playerCashlog);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            playerCashlogRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<PlayerCashlogDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PlayerCashlogDto playerCashlog : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("会员id", playerCashlog.getMemberId());
            map.put("玩具编号", playerCashlog.getToyNo());
            map.put("操作员id", playerCashlog.getUserId());
            map.put("店铺id", playerCashlog.getShopId());
            map.put("是否可用", playerCashlog.getEnabled());
            map.put("创建时间", playerCashlog.getCreateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}