/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author huangxiaoyu
* @date 2021-04-29
**/
@Entity
@Data
@Table(name="big_time_member")
public class Member implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "id")
    private Long id;

    @Column(name = "name")
    @ApiModelProperty(value = "儿童姓名")
    private String name;

    @Column(name = "gender")
    @ApiModelProperty(value = "性别")
    private String gender;

    @Column(name = "age")
    @ApiModelProperty(value = "年龄")
    private Integer age;

    @Column(name = "parents_name")
    @ApiModelProperty(value = "父母姓名")
    private String parentsName;

    @Column(name = "phone")
    @ApiModelProperty(value = "练习方式")
    private String phone;

    @Column(name = "other_name")
    @ApiModelProperty(value = "第二联系人姓名")
    private String otherName;

    @Column(name = "other_phone")
    @ApiModelProperty(value = "第二联系人电话")
    private String otherPhone;

    @Column(name = "create_id")
    @ApiModelProperty(value = "创建者id")
    private Long createId;

    @Column(name = "shop_id")
    @ApiModelProperty(value = "店铺名称")
    private Long shopId;

    @Column(name = "use_time")
    @ApiModelProperty(value = "用时")
    private Integer useTime;

    @Column(name = "total_time")
    @ApiModelProperty(value = "总时长")
    private Integer totalTime;

    @Column(name = "buy_time")
    @ApiModelProperty(value = "购买总时长")
    private Integer buyTime;

    @Column(name = "free_time")
    @ApiModelProperty(value = "免费总时长")
    private Integer freeTime;

    @Column(name = "status")
    @ApiModelProperty(value = "状态")
    private Integer status;

    @Column(name = "enabled")
    @ApiModelProperty(value = "是否启用")
    private Integer enabled;

    @Column(name = "update_by")
    @ApiModelProperty(value = "谁修改数据")
    private String updateBy;

    @Column(name = "update_time")
    @ApiModelProperty(value = "修改时间")
    private Timestamp updateTime;

    @Column(name = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Timestamp createTime;

    public void copy(Member source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}