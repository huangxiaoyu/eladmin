/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.impl;

import com.bigtime.domain.PlayerCashlogDetail;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.bigtime.repository.PlayerCashlogDetailRepository;
import com.bigtime.service.PlayerCashlogDetailService;
import com.bigtime.service.dto.PlayerCashlogDetailDto;
import com.bigtime.service.dto.PlayerCashlogDetailQueryCriteria;
import com.bigtime.service.mapstruct.PlayerCashlogDetailMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author huangxiaoyu
* @date 2021-04-29
**/
@Service
@RequiredArgsConstructor
public class PlayerCashlogDetailServiceImpl implements PlayerCashlogDetailService {

    private final PlayerCashlogDetailRepository playerCashlogDetailRepository;
    private final PlayerCashlogDetailMapper playerCashlogDetailMapper;

    @Override
    public Map<String,Object> queryAll(PlayerCashlogDetailQueryCriteria criteria, Pageable pageable){
        Page<PlayerCashlogDetail> page = playerCashlogDetailRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(playerCashlogDetailMapper::toDto));
    }

    @Override
    public List<PlayerCashlogDetailDto> queryAll(PlayerCashlogDetailQueryCriteria criteria){
        return playerCashlogDetailMapper.toDto(playerCashlogDetailRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public PlayerCashlogDetailDto findById(Long id) {
        PlayerCashlogDetail playerCashlogDetail = playerCashlogDetailRepository.findById(id).orElseGet(PlayerCashlogDetail::new);
        ValidationUtil.isNull(playerCashlogDetail.getId(),"PlayerCashlogDetail","id",id);
        return playerCashlogDetailMapper.toDto(playerCashlogDetail);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PlayerCashlogDetailDto create(PlayerCashlogDetail resources) {
        return playerCashlogDetailMapper.toDto(playerCashlogDetailRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PlayerCashlogDetail resources) {
        PlayerCashlogDetail playerCashlogDetail = playerCashlogDetailRepository.findById(resources.getId()).orElseGet(PlayerCashlogDetail::new);
        ValidationUtil.isNull( playerCashlogDetail.getId(),"PlayerCashlogDetail","id",resources.getId());
        playerCashlogDetail.copy(resources);
        playerCashlogDetailRepository.save(playerCashlogDetail);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            playerCashlogDetailRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<PlayerCashlogDetailDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PlayerCashlogDetailDto playerCashlogDetail : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("主记录id", playerCashlogDetail.getPlayerId());
            map.put("事件", playerCashlogDetail.getAction());
            map.put("描述", playerCashlogDetail.getDesc());
            map.put("是否可用", playerCashlogDetail.getEnabled());
            map.put("创建时间", playerCashlogDetail.getCreateDate());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}