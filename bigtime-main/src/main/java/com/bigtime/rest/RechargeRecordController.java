/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.rest;

import me.zhengjie.annotation.Log;
import com.bigtime.domain.RechargeRecord;
import com.bigtime.service.RechargeRecordService;
import com.bigtime.service.dto.RechargeRecordQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author huangxiaoyu
* @date 2021-04-29
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "业务:续费记录")
@RequestMapping("/api/rechargeRecord")
public class RechargeRecordController {

    private final RechargeRecordService rechargeRecordService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('rechargeRecord:list')")
    public void download(HttpServletResponse response, RechargeRecordQueryCriteria criteria) throws IOException {
        rechargeRecordService.download(rechargeRecordService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询rechargerecord")
    @ApiOperation("查询rechargerecord")
    @PreAuthorize("@el.check('rechargeRecord:list')")
    public ResponseEntity<Object> query(RechargeRecordQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(rechargeRecordService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增rechargerecord")
    @ApiOperation("新增rechargerecord")
    @PreAuthorize("@el.check('rechargeRecord:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody RechargeRecord resources){
        return new ResponseEntity<>(rechargeRecordService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改rechargerecord")
    @ApiOperation("修改rechargerecord")
    @PreAuthorize("@el.check('rechargeRecord:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody RechargeRecord resources){
        rechargeRecordService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除rechargerecord")
    @ApiOperation("删除rechargerecord")
    @PreAuthorize("@el.check('rechargeRecord:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        rechargeRecordService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}