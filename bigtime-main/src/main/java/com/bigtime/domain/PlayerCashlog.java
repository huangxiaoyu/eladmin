/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModelProperty;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @website https://el-admin.vip
* @description /
* @author huangxiaoyu
* @date 2021-04-29
**/
@Entity
@Data
@Table(name="big_time_player_cashlog")
public class PlayerCashlog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @ApiModelProperty(value = "id")
    private Long id;

    @Column(name = "member_id")
    @ApiModelProperty(value = "会员id")
    private Long memberId;

    @Column(name = "toy_no")
    @ApiModelProperty(value = "玩具编号")
    private Long toyNo;

    @Column(name = "user_id")
    @ApiModelProperty(value = "操作员id")
    private Long userId;

    @Column(name = "shop_id")
    @ApiModelProperty(value = "店铺id")
    private Long shopId;

    @Column(name = "enabled")
    @ApiModelProperty(value = "是否可用")
    private Integer enabled;

    @Column(name = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Timestamp createTime;

    public void copy(PlayerCashlog source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}