/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.impl;

import com.bigtime.domain.Staff;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.bigtime.repository.StaffRepository;
import com.bigtime.service.StaffService;
import com.bigtime.service.dto.StaffDto;
import com.bigtime.service.dto.StaffQueryCriteria;
import com.bigtime.service.mapstruct.StaffMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author huangxiaoyu
* @date 2021-04-29
**/
@Service
@RequiredArgsConstructor
public class StaffServiceImpl implements StaffService {

    private final StaffRepository staffRepository;
    private final StaffMapper staffMapper;

    @Override
    public Map<String,Object> queryAll(StaffQueryCriteria criteria, Pageable pageable){
        Page<Staff> page = staffRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(staffMapper::toDto));
    }

    @Override
    public List<StaffDto> queryAll(StaffQueryCriteria criteria){
        return staffMapper.toDto(staffRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public StaffDto findById(Long id) {
        Staff staff = staffRepository.findById(id).orElseGet(Staff::new);
        ValidationUtil.isNull(staff.getId(),"Staff","id",id);
        return staffMapper.toDto(staff);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public StaffDto create(Staff resources) {
        return staffMapper.toDto(staffRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Staff resources) {
        Staff staff = staffRepository.findById(resources.getId()).orElseGet(Staff::new);
        ValidationUtil.isNull( staff.getId(),"Staff","id",resources.getId());
        staff.copy(resources);
        staffRepository.save(staff);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            staffRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<StaffDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (StaffDto staff : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("姓名", staff.getName());
            map.put("电话", staff.getPhone());
            map.put("店铺名称", staff.getShop());
            map.put("店铺id", staff.getShopId());
            map.put("是否可用", staff.getEnabled());
            map.put("创建时间", staff.getCreateDate());
            map.put("角色id0", staff.getRoleId());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}