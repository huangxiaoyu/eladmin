/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.rest;

import me.zhengjie.annotation.Log;
import com.bigtime.domain.Combo;
import com.bigtime.service.ComboService;
import com.bigtime.service.dto.ComboQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author huangxiaoyu
* @date 2021-04-29
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "业务:套餐管理")
@RequestMapping("/api/combo")
public class ComboController {

    private final ComboService comboService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('combo:list')")
    public void download(HttpServletResponse response, ComboQueryCriteria criteria) throws IOException {
        comboService.download(comboService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询combo")
    @ApiOperation("查询combo")
    @PreAuthorize("@el.check('combo:list')")
    public ResponseEntity<Object> query(ComboQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(comboService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增combo")
    @ApiOperation("新增combo")
    @PreAuthorize("@el.check('combo:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody Combo resources){
        return new ResponseEntity<>(comboService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改combo")
    @ApiOperation("修改combo")
    @PreAuthorize("@el.check('combo:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody Combo resources){
        comboService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除combo")
    @ApiOperation("删除combo")
    @PreAuthorize("@el.check('combo:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        comboService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}