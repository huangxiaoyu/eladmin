/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : bigtime

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 30/04/2021 14:32:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for big_time_combo
-- ----------------------------
DROP TABLE IF EXISTS `big_time_combo`;
CREATE TABLE `big_time_combo`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `time` datetime NULL DEFAULT NULL COMMENT '时长',
  `free_time` datetime NULL DEFAULT NULL COMMENT '赠送时长',
  `push_money` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '提成',
  `shop_id` bigint NULL DEFAULT NULL COMMENT '店铺id',
  `enabled` int NULL DEFAULT NULL COMMENT '是否启用',
  `udpate_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '谁修改的',
  `udpate_date` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of big_time_combo
-- ----------------------------

-- ----------------------------
-- Table structure for big_time_member
-- ----------------------------
DROP TABLE IF EXISTS `big_time_member`;
CREATE TABLE `big_time_member`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '儿童姓名',
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '性别',
  `age` int NULL DEFAULT NULL COMMENT '年龄',
  `parents_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '父母姓名',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '练习方式',
  `other_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '第二联系人姓名',
  `other_phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '第二联系人电话',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建者id',
  `shop_id` bigint NULL DEFAULT NULL COMMENT '店铺名称',
  `use_time` int NULL DEFAULT NULL COMMENT '用时',
  `total_time` int NULL DEFAULT NULL COMMENT '总时长',
  `buy_time` int NULL DEFAULT NULL COMMENT '购买总时长',
  `free_time` int NULL DEFAULT NULL COMMENT '免费总时长',
  `status` int NULL DEFAULT NULL COMMENT '状态',
  `enabled` int NULL DEFAULT NULL COMMENT '是否启用',
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '谁修改数据',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of big_time_member
-- ----------------------------

-- ----------------------------
-- Table structure for big_time_player_cashlog
-- ----------------------------
DROP TABLE IF EXISTS `big_time_player_cashlog`;
CREATE TABLE `big_time_player_cashlog`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `member_id` bigint NULL DEFAULT NULL COMMENT '会员id',
  `toy_no` bigint NULL DEFAULT NULL COMMENT '玩具编号',
  `user_id` bigint NULL DEFAULT NULL COMMENT '操作员id',
  `shop_id` bigint NULL DEFAULT NULL COMMENT '店铺id',
  `enabled` int NULL DEFAULT NULL COMMENT '是否可用',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of big_time_player_cashlog
-- ----------------------------

-- ----------------------------
-- Table structure for big_time_player_cashlog_detail
-- ----------------------------
DROP TABLE IF EXISTS `big_time_player_cashlog_detail`;
CREATE TABLE `big_time_player_cashlog_detail`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `player_id` bigint NULL DEFAULT NULL COMMENT '主记录id',
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '事件',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `enabled` int NULL DEFAULT NULL COMMENT '是否可用',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of big_time_player_cashlog_detail
-- ----------------------------

-- ----------------------------
-- Table structure for big_time_recharge_record
-- ----------------------------
DROP TABLE IF EXISTS `big_time_recharge_record`;
CREATE TABLE `big_time_recharge_record`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `combo_id` bigint NULL DEFAULT NULL COMMENT '套餐id',
  `member_id` bigint NULL DEFAULT NULL COMMENT '会员id',
  `user_id` bigint NULL DEFAULT NULL COMMENT '操作员id',
  `shop_id` bigint NULL DEFAULT NULL COMMENT '店铺id',
  `push_money` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '提成',
  `enabled` int NULL DEFAULT NULL COMMENT '是否启用',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of big_time_recharge_record
-- ----------------------------

-- ----------------------------
-- Table structure for big_time_shop
-- ----------------------------
DROP TABLE IF EXISTS `big_time_shop`;
CREATE TABLE `big_time_shop`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '名称',
  `table` int NULL DEFAULT NULL COMMENT '桌子数量',
  `user_id` bigint NULL DEFAULT NULL COMMENT '负责人',
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电话',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地址',
  `status` int NULL DEFAULT NULL COMMENT '状态',
  `enabled` int NULL DEFAULT NULL COMMENT '是否可用',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of big_time_shop
-- ----------------------------

-- ----------------------------
-- Table structure for big_time_staff
-- ----------------------------
DROP TABLE IF EXISTS `big_time_staff`;
CREATE TABLE `big_time_staff`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '姓名',
  `phone` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '电话',
  `shop` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '店铺名称',
  `shop_id` bigint NULL DEFAULT NULL COMMENT '店铺id',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色id0',
  `enabled` int NULL DEFAULT NULL COMMENT '是否可用',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of big_time_staff
-- ----------------------------

-- ----------------------------
-- Table structure for big_time_toy
-- ----------------------------
DROP TABLE IF EXISTS `big_time_toy`;
CREATE TABLE `big_time_toy`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `toy_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '玩具编号',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '姓名',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `grain` int NULL DEFAULT NULL COMMENT '颗粒数',
  `age_group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '适合年龄段',
  `level` int NULL DEFAULT NULL COMMENT '难度等级',
  `enabled` int NULL DEFAULT NULL COMMENT '是否用',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of big_time_toy
-- ----------------------------

-- ----------------------------
-- Table structure for code_column_config
-- ----------------------------
DROP TABLE IF EXISTS `code_column_config`;
CREATE TABLE `code_column_config`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `column_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `column_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dict_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `extra` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `form_show` bit(1) NULL DEFAULT NULL,
  `form_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `key_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `list_show` bit(1) NULL DEFAULT NULL,
  `not_null` bit(1) NULL DEFAULT NULL,
  `query_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `date_annotation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`column_id`) USING BTREE,
  INDEX `idx_table_name`(`table_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 287 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成字段信息存储' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of code_column_config
-- ----------------------------
INSERT INTO `code_column_config` VALUES (191, 'sys_dept', 'dept_id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'0', NULL, 'ID', NULL);
INSERT INTO `code_column_config` VALUES (192, 'sys_dept', 'pid', 'bigint', NULL, '', b'1', NULL, 'MUL', b'1', b'0', NULL, '上级部门', NULL);
INSERT INTO `code_column_config` VALUES (193, 'sys_dept', 'sub_count', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '子部门数目', NULL);
INSERT INTO `code_column_config` VALUES (194, 'sys_dept', 'name', 'varchar', NULL, '', b'1', NULL, '', b'1', b'1', NULL, '名称', NULL);
INSERT INTO `code_column_config` VALUES (195, 'sys_dept', 'dept_sort', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '排序', NULL);
INSERT INTO `code_column_config` VALUES (196, 'sys_dept', 'enabled', 'bit', NULL, '', b'1', NULL, 'MUL', b'1', b'1', NULL, '状态', NULL);
INSERT INTO `code_column_config` VALUES (197, 'sys_dept', 'create_by', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建者', NULL);
INSERT INTO `code_column_config` VALUES (198, 'sys_dept', 'update_by', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '更新者', NULL);
INSERT INTO `code_column_config` VALUES (199, 'sys_dept', 'create_time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建日期', NULL);
INSERT INTO `code_column_config` VALUES (200, 'sys_dept', 'update_time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '更新时间', NULL);
INSERT INTO `code_column_config` VALUES (201, 'mnt_deploy', 'deploy_id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'0', NULL, 'ID', NULL);
INSERT INTO `code_column_config` VALUES (202, 'mnt_deploy', 'app_id', 'bigint', NULL, '', b'1', NULL, 'MUL', b'1', b'0', NULL, '应用编号', NULL);
INSERT INTO `code_column_config` VALUES (203, 'mnt_deploy', 'create_by', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建者', NULL);
INSERT INTO `code_column_config` VALUES (204, 'mnt_deploy', 'update_by', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '更新者', NULL);
INSERT INTO `code_column_config` VALUES (205, 'mnt_deploy', 'create_time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '', NULL);
INSERT INTO `code_column_config` VALUES (206, 'mnt_deploy', 'update_time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '更新时间', NULL);
INSERT INTO `code_column_config` VALUES (207, 'sys_dept', 'user_id', 'bigint', NULL, '', b'1', 'Input', '', b'1', b'0', NULL, '部门领导', NULL);
INSERT INTO `code_column_config` VALUES (208, 'big_time_shop', 'id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'0', NULL, '', NULL);
INSERT INTO `code_column_config` VALUES (209, 'big_time_shop', 'name', 'varchar', NULL, '', b'1', NULL, '', b'1', b'1', 'Like', '名称', NULL);
INSERT INTO `code_column_config` VALUES (210, 'big_time_shop', 'table', 'int', NULL, '', b'1', NULL, '', b'1', b'1', NULL, '桌子数量', NULL);
INSERT INTO `code_column_config` VALUES (211, 'big_time_shop', 'user_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '负责人', NULL);
INSERT INTO `code_column_config` VALUES (212, 'big_time_shop', 'phone', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '电话', NULL);
INSERT INTO `code_column_config` VALUES (213, 'big_time_shop', 'address', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '地址', NULL);
INSERT INTO `code_column_config` VALUES (214, 'big_time_shop', 'status', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '状态', NULL);
INSERT INTO `code_column_config` VALUES (216, 'big_time_shop', 'create_date', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建时间', NULL);
INSERT INTO `code_column_config` VALUES (217, 'big_time_shop', 'enabled', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '是否可用', NULL);
INSERT INTO `code_column_config` VALUES (218, 'big_time_combo', 'id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'1', NULL, '', NULL);
INSERT INTO `code_column_config` VALUES (219, 'big_time_combo', 'name', 'varchar', NULL, '', b'1', 'Input', '', b'1', b'0', 'Like', '名称\r\n\r\n', NULL);
INSERT INTO `code_column_config` VALUES (220, 'big_time_combo', 'price', 'decimal', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '价格', NULL);
INSERT INTO `code_column_config` VALUES (221, 'big_time_combo', 'time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '时长', NULL);
INSERT INTO `code_column_config` VALUES (222, 'big_time_combo', 'free_time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '赠送时长', NULL);
INSERT INTO `code_column_config` VALUES (223, 'big_time_combo', 'push_money', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '提成', NULL);
INSERT INTO `code_column_config` VALUES (224, 'big_time_combo', 'shop_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '店铺id', NULL);
INSERT INTO `code_column_config` VALUES (225, 'big_time_combo', 'enabled', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '是否启用', NULL);
INSERT INTO `code_column_config` VALUES (226, 'big_time_combo', 'udpate_by', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '谁修改的', NULL);
INSERT INTO `code_column_config` VALUES (227, 'big_time_combo', 'udpate_date', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '修改时间', NULL);
INSERT INTO `code_column_config` VALUES (228, 'big_time_combo', 'create_date', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建时间', NULL);
INSERT INTO `code_column_config` VALUES (229, 'big_time_recharge_record', 'id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'0', NULL, '', NULL);
INSERT INTO `code_column_config` VALUES (230, 'big_time_recharge_record', 'combo_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '套餐id', NULL);
INSERT INTO `code_column_config` VALUES (231, 'big_time_recharge_record', 'member_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '会员id', NULL);
INSERT INTO `code_column_config` VALUES (232, 'big_time_recharge_record', 'user_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '操作员id', NULL);
INSERT INTO `code_column_config` VALUES (233, 'big_time_recharge_record', 'shop_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '店铺id', NULL);
INSERT INTO `code_column_config` VALUES (234, 'big_time_recharge_record', 'push_money', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '提成', NULL);
INSERT INTO `code_column_config` VALUES (235, 'big_time_recharge_record', 'enabled', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '是否启用', NULL);
INSERT INTO `code_column_config` VALUES (236, 'big_time_recharge_record', 'create_date', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建时间', NULL);
INSERT INTO `code_column_config` VALUES (237, 'big_time_toy', 'id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'0', NULL, '', NULL);
INSERT INTO `code_column_config` VALUES (238, 'big_time_toy', 'toy_no', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '玩具编号', NULL);
INSERT INTO `code_column_config` VALUES (239, 'big_time_toy', 'name', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '姓名', NULL);
INSERT INTO `code_column_config` VALUES (240, 'big_time_toy', 'price', 'decimal', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '价格', NULL);
INSERT INTO `code_column_config` VALUES (241, 'big_time_toy', 'grain', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '颗粒数', NULL);
INSERT INTO `code_column_config` VALUES (242, 'big_time_toy', 'age_group', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '适合年龄段', NULL);
INSERT INTO `code_column_config` VALUES (243, 'big_time_toy', 'level', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '难度等级', NULL);
INSERT INTO `code_column_config` VALUES (244, 'big_time_toy', 'enabled', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '是否用', NULL);
INSERT INTO `code_column_config` VALUES (245, 'big_time_toy', 'create_time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建时间', NULL);
INSERT INTO `code_column_config` VALUES (246, 'big_time_staff', 'id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'1', NULL, '', NULL);
INSERT INTO `code_column_config` VALUES (247, 'big_time_staff', 'name', 'varchar', NULL, '', b'1', 'Input', '', b'1', b'0', 'Like', '姓名', NULL);
INSERT INTO `code_column_config` VALUES (248, 'big_time_staff', 'phone', 'varchar', NULL, '', b'1', 'Input', '', b'1', b'0', 'Like', '电话', NULL);
INSERT INTO `code_column_config` VALUES (249, 'big_time_staff', 'shop', 'varchar', NULL, '', b'1', 'Input', '', b'1', b'0', 'Like', '店铺名称', NULL);
INSERT INTO `code_column_config` VALUES (250, 'big_time_staff', 'shop_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '店铺id', NULL);
INSERT INTO `code_column_config` VALUES (252, 'big_time_staff', 'enabled', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '是否可用', NULL);
INSERT INTO `code_column_config` VALUES (253, 'big_time_staff', 'create_date', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建时间', NULL);
INSERT INTO `code_column_config` VALUES (254, 'big_time_member', 'id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'0', NULL, '', NULL);
INSERT INTO `code_column_config` VALUES (255, 'big_time_member', 'name', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '儿童姓名', NULL);
INSERT INTO `code_column_config` VALUES (256, 'big_time_member', 'gender', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '性别', NULL);
INSERT INTO `code_column_config` VALUES (257, 'big_time_member', 'age', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '年龄', NULL);
INSERT INTO `code_column_config` VALUES (258, 'big_time_member', 'parents_name', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '父母姓名', NULL);
INSERT INTO `code_column_config` VALUES (259, 'big_time_member', 'phone', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '练习方式', NULL);
INSERT INTO `code_column_config` VALUES (260, 'big_time_member', 'other_name', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '第二联系人姓名', NULL);
INSERT INTO `code_column_config` VALUES (261, 'big_time_member', 'other_phone', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '第二联系人电话', NULL);
INSERT INTO `code_column_config` VALUES (262, 'big_time_member', 'create_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建者id', NULL);
INSERT INTO `code_column_config` VALUES (263, 'big_time_member', 'shop_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '店铺名称', NULL);
INSERT INTO `code_column_config` VALUES (264, 'big_time_member', 'use_time', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '用时', NULL);
INSERT INTO `code_column_config` VALUES (265, 'big_time_member', 'total_time', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '总时长', NULL);
INSERT INTO `code_column_config` VALUES (266, 'big_time_member', 'buy_time', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '购买总时长', NULL);
INSERT INTO `code_column_config` VALUES (267, 'big_time_member', 'free_time', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '免费总时长', NULL);
INSERT INTO `code_column_config` VALUES (268, 'big_time_member', 'status', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '状态', NULL);
INSERT INTO `code_column_config` VALUES (269, 'big_time_member', 'enabled', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '是否启用', NULL);
INSERT INTO `code_column_config` VALUES (270, 'big_time_member', 'update_by', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '谁修改数据', NULL);
INSERT INTO `code_column_config` VALUES (271, 'big_time_member', 'update_time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '修改时间', NULL);
INSERT INTO `code_column_config` VALUES (272, 'big_time_member', 'create_time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建时间', NULL);
INSERT INTO `code_column_config` VALUES (273, 'big_time_player_cashlog', 'id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'1', NULL, '', NULL);
INSERT INTO `code_column_config` VALUES (274, 'big_time_player_cashlog', 'member_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '会员id', NULL);
INSERT INTO `code_column_config` VALUES (275, 'big_time_player_cashlog', 'toy_no', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '玩具编号', NULL);
INSERT INTO `code_column_config` VALUES (276, 'big_time_player_cashlog', 'user_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '操作员id', NULL);
INSERT INTO `code_column_config` VALUES (277, 'big_time_player_cashlog', 'shop_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '店铺id', NULL);
INSERT INTO `code_column_config` VALUES (278, 'big_time_player_cashlog', 'enabled', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '是否可用', NULL);
INSERT INTO `code_column_config` VALUES (279, 'big_time_player_cashlog', 'create_time', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建时间', NULL);
INSERT INTO `code_column_config` VALUES (280, 'big_time_player_cashlog_detail', 'id', 'bigint', NULL, 'auto_increment', b'1', NULL, 'PRI', b'1', b'1', NULL, '', NULL);
INSERT INTO `code_column_config` VALUES (281, 'big_time_player_cashlog_detail', 'player_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '主记录id', NULL);
INSERT INTO `code_column_config` VALUES (282, 'big_time_player_cashlog_detail', 'action', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '事件', NULL);
INSERT INTO `code_column_config` VALUES (283, 'big_time_player_cashlog_detail', 'desc', 'varchar', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '描述', NULL);
INSERT INTO `code_column_config` VALUES (284, 'big_time_player_cashlog_detail', 'enabled', 'int', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '是否可用', NULL);
INSERT INTO `code_column_config` VALUES (285, 'big_time_player_cashlog_detail', 'create_date', 'datetime', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '创建时间', NULL);
INSERT INTO `code_column_config` VALUES (286, 'big_time_staff', 'role_id', 'bigint', NULL, '', b'1', NULL, '', b'1', b'0', NULL, '角色id0', NULL);

-- ----------------------------
-- Table structure for code_gen_config
-- ----------------------------
DROP TABLE IF EXISTS `code_gen_config`;
CREATE TABLE `code_gen_config`  (
  `config_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表名',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `cover` bit(1) NULL DEFAULT NULL COMMENT '是否覆盖',
  `module_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模块名称',
  `pack` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '至于哪个包下',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '前端代码生成的路径',
  `api_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '前端Api文件路径',
  `prefix` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '表前缀',
  `api_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '接口名称',
  PRIMARY KEY (`config_id`) USING BTREE,
  INDEX `idx_table_name`(`table_name`(100)) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '代码生成器配置' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of code_gen_config
-- ----------------------------
INSERT INTO `code_gen_config` VALUES (7, 'sys_dept', 'Zheng Jie', b'0', 'eladmin-system', 'me.zhengjie.modules.system.domain', '/system/dept', '/system/dept\\', NULL, 'dept');
INSERT INTO `code_gen_config` VALUES (8, 'big_time_shop', 'huangxiaoyu', b'1', 'bigtime-main', 'com.bigtime', 'bigtime/shop', 'bigtime/shop\\', 'big_time_', 'shop');
INSERT INTO `code_gen_config` VALUES (9, 'big_time_combo', 'huangxiaoyu', b'0', 'bigtime-main', 'com.bigtime', 'bigtime/combo', 'bigtime/combo\\', 'big_time_', 'combo');
INSERT INTO `code_gen_config` VALUES (10, 'big_time_member', 'huangxiaoyu', b'0', 'bigtime-main', 'com.bigtime', 'bigtime/member', 'bigtime/member\\', 'big_time_', 'member');
INSERT INTO `code_gen_config` VALUES (11, 'big_time_player_cashlog', 'huangxiaoyu', b'0', 'bigtime-main', 'com.bigtime', 'bigtime/playercashlog', 'bigtime/playercashlog\\', 'big_time_', 'playercashlog');
INSERT INTO `code_gen_config` VALUES (12, 'big_time_player_cashlog_detail', 'huangxiaoyu', b'0', 'bigtime-main', 'com.bigtime', 'bigtime/playercashlogdetail', 'bigtime/playercashlogdetail\\', 'big_time_', 'playercashlogdetail');
INSERT INTO `code_gen_config` VALUES (13, 'big_time_recharge_record', 'huangxiaoyu', b'0', 'bigtime-main', 'com.bigtime', 'bigtime/rechargerecord', 'bigtime/rechargerecord\\', 'big_time_', 'rechargerecord');
INSERT INTO `code_gen_config` VALUES (14, 'big_time_toy', 'huangxiaoyu', b'0', 'bigtime-main', 'com.bigtime', 'bigtime/toy', 'bigtime/toy\\', 'big_time_', 'toy');
INSERT INTO `code_gen_config` VALUES (15, 'big_time_staff', 'huangxiaoyu', b'0', 'bigtime-main', 'com.bigtime', 'bigtime/staff', 'bigtime/staff\\', 'big_time_', 'staff');

-- ----------------------------
-- Table structure for mnt_app
-- ----------------------------
DROP TABLE IF EXISTS `mnt_app`;
CREATE TABLE `mnt_app`  (
  `app_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用名称',
  `upload_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '上传目录',
  `deploy_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部署路径',
  `backup_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备份路径',
  `port` int NULL DEFAULT NULL COMMENT '应用端口',
  `start_script` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '启动脚本',
  `deploy_script` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '部署脚本',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`app_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of mnt_app
-- ----------------------------

-- ----------------------------
-- Table structure for mnt_database
-- ----------------------------
DROP TABLE IF EXISTS `mnt_database`;
CREATE TABLE `mnt_database`  (
  `db_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `jdbc_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'jdbc连接',
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '账号',
  `pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`db_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据库管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of mnt_database
-- ----------------------------

-- ----------------------------
-- Table structure for mnt_deploy
-- ----------------------------
DROP TABLE IF EXISTS `mnt_deploy`;
CREATE TABLE `mnt_deploy`  (
  `deploy_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `app_id` bigint NULL DEFAULT NULL COMMENT '应用编号',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`deploy_id`) USING BTREE,
  INDEX `FK6sy157pseoxx4fmcqr1vnvvhy`(`app_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部署管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of mnt_deploy
-- ----------------------------

-- ----------------------------
-- Table structure for mnt_deploy_history
-- ----------------------------
DROP TABLE IF EXISTS `mnt_deploy_history`;
CREATE TABLE `mnt_deploy_history`  (
  `history_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT 'ID',
  `app_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '应用名称',
  `deploy_date` datetime NOT NULL COMMENT '部署日期',
  `deploy_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部署用户',
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '服务器IP',
  `deploy_id` bigint NULL DEFAULT NULL COMMENT '部署编号',
  PRIMARY KEY (`history_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部署历史管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of mnt_deploy_history
-- ----------------------------

-- ----------------------------
-- Table structure for mnt_deploy_server
-- ----------------------------
DROP TABLE IF EXISTS `mnt_deploy_server`;
CREATE TABLE `mnt_deploy_server`  (
  `deploy_id` bigint NOT NULL COMMENT '部署ID',
  `server_id` bigint NOT NULL COMMENT '服务ID',
  PRIMARY KEY (`deploy_id`, `server_id`) USING BTREE,
  INDEX `FKeaaha7jew9a02b3bk9ghols53`(`server_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '应用与服务器关联' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of mnt_deploy_server
-- ----------------------------

-- ----------------------------
-- Table structure for mnt_server
-- ----------------------------
DROP TABLE IF EXISTS `mnt_server`;
CREATE TABLE `mnt_server`  (
  `server_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `account` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '账号',
  `ip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'IP地址',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `port` int NULL DEFAULT NULL COMMENT '端口',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`server_id`) USING BTREE,
  INDEX `idx_ip`(`ip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '服务器管理' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of mnt_server
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` bigint NULL DEFAULT NULL COMMENT '上级部门',
  `user_id` bigint NULL DEFAULT NULL COMMENT '部门领导',
  `sub_count` int NULL DEFAULT 0 COMMENT '子部门数目',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `dept_sort` int NULL DEFAULT 999 COMMENT '排序',
  `enabled` bit(1) NOT NULL COMMENT '状态',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE,
  INDEX `inx_pid`(`pid`) USING BTREE,
  INDEX `inx_enabled`(`enabled`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (2, 7, NULL, 1, '郑州', 3, b'1', 'admin', 'admin', '2019-03-25 09:15:32', '2021-04-29 10:10:00');
INSERT INTO `sys_dept` VALUES (5, 7, NULL, 0, '金水区', 4, b'1', 'admin', 'admin', '2019-03-25 09:20:44', '2021-04-29 10:10:08');
INSERT INTO `sys_dept` VALUES (7, NULL, NULL, 2, '河南', 0, b'1', 'admin', 'admin', '2019-03-25 11:04:50', '2021-04-29 10:09:51');
INSERT INTO `sys_dept` VALUES (17, 2, NULL, 0, '研发一组', 999, b'1', 'admin', 'admin', '2020-08-02 14:49:07', '2020-08-02 14:49:07');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES (1, 'user_status', '用户状态', NULL, NULL, '2019-10-27 20:31:36', NULL);
INSERT INTO `sys_dict` VALUES (4, 'dept_status', '部门状态', NULL, NULL, '2019-10-27 20:31:36', NULL);
INSERT INTO `sys_dict` VALUES (5, 'job_status', '岗位状态', NULL, NULL, '2019-10-27 20:31:36', NULL);

-- ----------------------------
-- Table structure for sys_dict_detail
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_detail`;
CREATE TABLE `sys_dict_detail`  (
  `detail_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dict_id` bigint NULL DEFAULT NULL COMMENT '字典id',
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典标签',
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典值',
  `dict_sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`detail_id`) USING BTREE,
  INDEX `FK5tpkputc6d9nboxojdbgnpmyb`(`dict_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典详情' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_dict_detail
-- ----------------------------
INSERT INTO `sys_dict_detail` VALUES (1, 1, '激活', 'true', 1, NULL, NULL, '2019-10-27 20:31:36', NULL);
INSERT INTO `sys_dict_detail` VALUES (2, 1, '禁用', 'false', 2, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_detail` VALUES (3, 4, '启用', 'true', 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_detail` VALUES (4, 4, '停用', 'false', 2, NULL, NULL, '2019-10-27 20:31:36', NULL);
INSERT INTO `sys_dict_detail` VALUES (5, 5, '启用', 'true', 1, NULL, NULL, NULL, NULL);
INSERT INTO `sys_dict_detail` VALUES (6, 5, '停用', 'false', 2, NULL, NULL, '2019-10-27 20:31:36', NULL);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '岗位名称',
  `enabled` bit(1) NOT NULL COMMENT '岗位状态',
  `job_sort` int NULL DEFAULT NULL COMMENT '排序',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`job_id`) USING BTREE,
  UNIQUE INDEX `uniq_name`(`name`) USING BTREE,
  INDEX `inx_enabled`(`enabled`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '岗位' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES (8, '人事专员', b'1', 3, NULL, NULL, '2019-03-29 14:52:28', NULL);
INSERT INTO `sys_job` VALUES (10, '产品经理', b'1', 4, NULL, NULL, '2019-03-29 14:55:51', NULL);
INSERT INTO `sys_job` VALUES (11, '全栈开发', b'1', 2, NULL, 'admin', '2019-03-31 13:39:30', '2021-04-29 09:44:32');
INSERT INTO `sys_job` VALUES (12, '软件测试', b'1', 5, NULL, 'admin', '2019-03-31 13:39:43', '2020-05-10 19:56:26');
INSERT INTO `sys_job` VALUES (13, '技师', b'1', 10, 'admin', 'admin', '2021-03-06 16:44:08', '2021-03-06 16:44:08');
INSERT INTO `sys_job` VALUES (14, '老板', b'1', 11, 'admin', 'admin', '2021-04-29 09:44:49', '2021-04-29 09:44:49');
INSERT INTO `sys_job` VALUES (15, '店铺经理', b'1', 12, 'admin', 'admin', '2021-04-29 09:44:57', '2021-04-29 09:44:57');
INSERT INTO `sys_job` VALUES (16, '职员', b'1', 13, 'admin', 'admin', '2021-04-29 09:45:04', '2021-04-29 09:45:04');
INSERT INTO `sys_job` VALUES (17, '会员', b'1', 999, 'admin', 'admin', '2021-04-30 08:38:38', '2021-04-30 08:38:38');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `log_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `log_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `params` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `request_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` bigint NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `browser` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `exception_detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `create_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`log_id`) USING BTREE,
  INDEX `log_create_time_index`(`create_time`) USING BTREE,
  INDEX `inx_log_type`(`log_type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3601 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统日志' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (3537, '修改角色', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.update()', '{\"level\":4,\"description\":\"-\",\"updateTime\":1599273912000,\"dataScope\":\"本级\",\"createTime\":1542949746000,\"name\":\"普通用户\",\"id\":2,\"menus\":[{\"cache\":false,\"hidden\":false,\"icon\":\"markdown\",\"pid\":10,\"title\":\"Markdown\",\"type\":1,\"subCount\":0,\"path\":\"markdown\",\"component\":\"components/MarkDown\",\"createTime\":1552024004000,\"iFrame\":false,\"id\":33,\"componentName\":\"Markdown\",\"menuSort\":53},{\"cache\":false,\"hidden\":false,\"icon\":\"system\",\"title\":\"系统管理\",\"type\":0,\"subCount\":7,\"path\":\"system\",\"createTime\":1545117089000,\"iFrame\":false,\"id\":1,\"menuSort\":1},{\"cache\":false,\"hidden\":false,\"icon\":\"dev\",\"pid\":10,\"title\":\"Yaml编辑器\",\"type\":1,\"subCount\":0,\"path\":\"yaml\",\"component\":\"components/YamlEdit\",\"createTime\":1552031380000,\"iFrame\":false,\"id\":34,\"componentName\":\"YamlEdit\",\"menuSort\":54},{\"cache\":false,\"hidden\":false,\"icon\":\"peoples\",\"permission\":\"user:list\",\"pid\":1,\"title\":\"用户管理\",\"type\":1,\"subCount\":3,\"path\":\"user\",\"component\":\"system/user/index\",\"createTime\":1545117284000,\"iFrame\":false,\"id\":2,\"componentName\":\"User\",\"menuSort\":2},{\"cache\":false,\"hidden\":false,\"icon\":\"sys-tools\",\"title\":\"系统工具\",\"type\":0,\"subCount\":7,\"path\":\"sys-tools\",\"component\":\"\",\"createTime\":1553828255000,\"iFrame\":false,\"id\":36,\"menuSort\":30},{\"cache\":false,\"hidden\":false,\"icon\":\"monitor\",\"title\":\"系统监控\",\"type\":0,\"subCount\":5,\"path\":\"monitor\",\"createTime\":1545117468000,\"iFrame\":false,\"id\":6,\"menuSort\":10},{\"cache\":true,\"hidden\":false,\"icon\":\"log\",\"pid\":6,\"updateTime\":1591420317000,\"title\":\"操作日志\",\"type\":1,\"subCount\":0,\"path\":\"logs\",\"component\":\"monitor/log/index\",\"createTime\":1545117506000,\"iFrame\":false,\"id\":7,\"componentName\":\"Log\",\"menuSort\":11},{\"cache\":false,\"hidden\":false,\"icon\":\"sqlMonitor\",\"pid\":6,\"title\":\"SQL监控\",\"type\":1,\"subCount\":0,\"path\":\"druid\",\"component\":\"monitor/sql/index\",\"createTime\":1545117574000,\"iFrame\":false,\"id\":9,\"componentName\":\"Sql\",\"menuSort\":18},{\"cache\":false,\"hidden\":false,\"icon\":\"zujian\",\"title\":\"组件管理\",\"type\":0,\"subCount\":5,\"path\":\"components\",\"createTime\":1545197896000,\"iFrame\":false,\"id\":10,\"menuSort\":50},{\"cache\":false,\"hidden\":false,\"icon\":\"icon\",\"pid\":10,\"title\":\"图标库\",\"type\":1,\"subCount\":0,\"path\":\"icon\",\"component\":\"components/icons/index\",\"createTime\":1545197929000,\"iFrame\":false,\"id\":11,\"componentName\":\"Icons\",\"menuSort\":51},{\"cache\":false,\"hidden\":false,\"icon\":\"email\",\"pid\":36,\"title\":\"邮件工具\",\"type\":1,\"subCount\":0,\"path\":\"email\",\"component\":\"tools/email/index\",\"createTime\":1545876789000,\"iFrame\":false,\"id\":14,\"componentName\":\"Email\",\"menuSort\":35},{\"cache\":false,\"hidden\":false,\"icon\":\"fwb\",\"pid\":10,\"title\":\"富文本\",\"type\":1,\"subCount\":0,\"path\":\"tinymce\",\"component\":\"components/Editor\",\"createTime\":1545883105000,\"iFrame\":false,\"id\":15,\"componentName\":\"Editor\",\"menuSort\":52},{\"cache\":false,\"hidden\":false,\"icon\":\"codeConsole\",\"permission\":\"monitor:list\",\"pid\":6,\"updateTime\":1588587650000,\"title\":\"服务监控\",\"type\":1,\"subCount\":0,\"path\":\"server\",\"component\":\"monitor/server/index\",\"createTime\":1573103199000,\"iFrame\":false,\"id\":80,\"componentName\":\"ServerMonitor\",\"menuSort\":14},{\"cache\":true,\"hidden\":true,\"icon\":\"dev\",\"permission\":\"\",\"pid\":36,\"title\":\"生成配置\",\"type\":1,\"subCount\":0,\"path\":\"generator/config/:tableName\",\"component\":\"generator/config\",\"createTime\":1573992536000,\"iFrame\":false,\"id\":82,\"componentName\":\"GeneratorConfig\",\"menuSort\":33},{\"cache\":false,\"hidden\":false,\"icon\":\"alipay\",\"pid\":36,\"title\":\"支付宝工具\",\"type\":1,\"subCount\":0,\"path\":\"aliPay\",\"component\":\"tools/aliPay/index\",\"createTime\":1546239158000,\"iFrame\":false,\"id\":19,\"componentName\":\"AliPay\",\"menuSort\":37},{\"cache\":true,\"hidden\":false,\"icon\":\"chart\",\"permission\":\"\",\"pid\":10,\"title\":\"图表库\",\"type\":1,\"subCount\":0,\"path\":\"echarts\",\"component\":\"components/Echarts\",\"createTime\":1574298272000,\"iFrame\":false,\"id\":83,\"componentName\":\"Echarts\",\"menuSort\":50},{\"cache\":true,\"hidden\":true,\"icon\":\"java\",\"pid\":36,\"title\":\"生成预览\",\"type\":1,\"subCount\":0,\"path\":\"generator/preview/:tableName\",\"component\":\"generator/preview\",\"createTime\":1574751276000,\"iFrame\":false,\"id\":116,\"componentName\":\"Preview\",\"menuSort\":999},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"updateTime\":1592731655000,\"title\":\"多级菜单\",\"type\":0,\"subCount\":2,\"path\":\"nested\",\"component\":\"\",\"createTime\":1546590123000,\"iFrame\":false,\"id\":21,\"menuSort\":900},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"pid\":21,\"updateTime\":1592731640000,\"title\":\"二级菜单1\",\"type\":0,\"subCount\":2,\"path\":\"menu1\",\"component\":\"\",\"createTime\":1546590209000,\"iFrame\":false,\"id\":22,\"menuSort\":999},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"pid\":21,\"title\":\"二级菜单2\",\"type\":1,\"subCount\":0,\"path\":\"menu2\",\"component\":\"nested/menu2/index\",\"createTime\":1546590237000,\"iFrame\":false,\"id\":23,\"menuSort\":999},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"pid\":22,\"title\":\"三级菜单1\",\"type\":1,\"subCount\":0,\"path\":\"menu1-1\",\"component\":\"nested/menu1/menu1-1\",\"createTime\":1546590288000,\"iFrame\":false,\"id\":24,\"componentName\":\"Test\",\"menuSort\":999},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"pid\":22,\"title\":\"三级菜单2\",\"type\":1,\"subCount\":0,\"path\":\"menu1-2\",\"component\":\"nested/menu1/menu1-2\",\"createTime\":1546853252000,\"iFrame\":false,\"id\":27,\"menuSort\":999},{\"cache\":true,\"hidden\":false,\"icon\":\"dev\",\"pid\":36,\"title\":\"代码生成\",\"type\":1,\"subCount\":0,\"path\":\"generator\",\"component\":\"generator/index\",\"createTime\":1547192755000,\"iFrame\":false,\"id\":30,\"componentName\":\"GeneratorIndex\",\"menuSort\":32},{\"cache\":false,\"hidden\":false,\"icon\":\"error\",\"pid\":6,\"title\":\"异常日志\",\"type\":1,\"subCount\":0,\"path\":\"errorLog\",\"component\":\"monitor/log/errorLog\",\"createTime\":1547358543000,\"iFrame\":false,\"id\":32,\"componentName\":\"ErrorLog\",\"menuSort\":12}],\"depts\":[]}', '192.168.3.109', 90, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-06 16:42:00');
INSERT INTO `sys_log` VALUES (3538, '新增岗位', 'INFO', 'me.zhengjie.modules.system.rest.JobController.create()', '{\"updateTime\":1615020247583,\"enabled\":true,\"jobSort\":10,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615020247583,\"name\":\"技师\",\"id\":13}', '192.168.3.109', 21, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-06 16:44:08');
INSERT INTO `sys_log` VALUES (3539, '新增部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.create()', '{\"updateTime\":1615172144923,\"enabled\":true,\"deptSort\":999,\"subCount\":0,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615172144923,\"name\":\"华强广场\",\"id\":18}', '192.168.244.1', 38, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-08 10:55:45');
INSERT INTO `sys_log` VALUES (3540, '新增部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.create()', '{\"updateTime\":1615172670573,\"enabled\":true,\"deptSort\":999,\"subCount\":0,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615172670573,\"name\":\"111\",\"id\":19}', '192.168.244.1', 7, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-08 11:04:31');
INSERT INTO `sys_log` VALUES (3541, '修改部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.update()', '{\"updateTime\":1615172671000,\"enabled\":true,\"deptSort\":999,\"subCount\":0,\"createBy\":\"admin\",\"createTime\":1615172671000,\"name\":\"111\",\"id\":19}', '192.168.244.1', 16, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-08 11:05:49');
INSERT INTO `sys_log` VALUES (3542, '删除部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.delete()', '[19]', '192.168.244.1', 87, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-08 11:10:38');
INSERT INTO `sys_log` VALUES (3543, '修改部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.update()', '{\"updateTime\":1615172145000,\"userId\":2,\"enabled\":true,\"deptSort\":999,\"subCount\":0,\"createBy\":\"admin\",\"createTime\":1615172145000,\"name\":\"华强广场\",\"id\":18}', '192.168.244.1', 44, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-08 11:10:42');
INSERT INTO `sys_log` VALUES (3544, '新增部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.create()', '{\"updateTime\":1615173050639,\"userId\":1231,\"enabled\":true,\"deptSort\":999,\"subCount\":0,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615173050639,\"name\":\"111\",\"id\":20}', '192.168.244.1', 5, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-08 11:10:51');
INSERT INTO `sys_log` VALUES (3545, '删除部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.delete()', '[20]', '192.168.244.1', 25, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-08 11:10:54');
INSERT INTO `sys_log` VALUES (3546, '新增菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.create()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"icon\",\"updateTime\":1615275125731,\"title\":\"数据管理\",\"type\":0,\"subCount\":0,\"path\":\"views/shop\",\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615275125731,\"iFrame\":false,\"id\":118,\"menuSort\":999}', '192.168.244.1', 40, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:32:06');
INSERT INTO `sys_log` VALUES (3547, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"icon\",\"updateTime\":1615275126000,\"title\":\"数据管理\",\"type\":0,\"subCount\":0,\"path\":\"shop\",\"createBy\":\"admin\",\"createTime\":1615275126000,\"iFrame\":false,\"id\":118,\"menuSort\":999}', '192.168.244.1', 15, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:36:51');
INSERT INTO `sys_log` VALUES (3548, '新增菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.create()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"user\",\"pid\":118,\"updateTime\":1615275504848,\"title\":\"会员管理\",\"type\":0,\"subCount\":0,\"path\":\"member\",\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615275504845,\"iFrame\":false,\"id\":119,\"menuSort\":999}', '192.168.244.1', 22, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:38:25');
INSERT INTO `sys_log` VALUES (3549, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"user\",\"pid\":118,\"updateTime\":1615275505000,\"title\":\"会员管理\",\"type\":1,\"subCount\":0,\"path\":\"member\",\"component\":\"member\",\"createBy\":\"admin\",\"createTime\":1615275505000,\"iFrame\":false,\"id\":119,\"componentName\":\"member\",\"menuSort\":999}', '192.168.244.1', 16, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:39:01');
INSERT INTO `sys_log` VALUES (3550, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"user\",\"permission\":\"member:list\",\"pid\":118,\"updateTime\":1615275541000,\"title\":\"会员管理\",\"type\":1,\"subCount\":0,\"path\":\"member\",\"component\":\"bigtime/member/index\",\"createBy\":\"admin\",\"createTime\":1615275505000,\"iFrame\":false,\"id\":119,\"componentName\":\"Member\",\"menuSort\":1}', '192.168.244.1', 16, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:40:20');
INSERT INTO `sys_log` VALUES (3551, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"user\",\"permission\":\"member:list\",\"pid\":118,\"updateTime\":1615275620000,\"title\":\"会员管理\",\"type\":1,\"subCount\":0,\"path\":\"member\",\"component\":\"bigtime/member/index\",\"createBy\":\"admin\",\"createTime\":1615275505000,\"iFrame\":false,\"id\":119,\"componentName\":\"Member\",\"menuSort\":1}', '192.168.244.1', 9, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:40:52');
INSERT INTO `sys_log` VALUES (3552, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":1,\"menus\":[{\"subCount\":0,\"id\":97,\"menuSort\":999},{\"subCount\":0,\"id\":98,\"menuSort\":999},{\"subCount\":0,\"id\":102,\"menuSort\":999},{\"subCount\":0,\"id\":103,\"menuSort\":999},{\"subCount\":0,\"id\":104,\"menuSort\":999},{\"subCount\":0,\"id\":105,\"menuSort\":999},{\"subCount\":0,\"id\":106,\"menuSort\":999},{\"subCount\":0,\"id\":107,\"menuSort\":999},{\"subCount\":0,\"id\":108,\"menuSort\":999},{\"subCount\":0,\"id\":109,\"menuSort\":999},{\"subCount\":0,\"id\":110,\"menuSort\":999},{\"subCount\":0,\"id\":111,\"menuSort\":999},{\"subCount\":0,\"id\":112,\"menuSort\":999},{\"subCount\":0,\"id\":113,\"menuSort\":999},{\"subCount\":0,\"id\":114,\"menuSort\":999},{\"subCount\":0,\"id\":116,\"menuSort\":999},{\"subCount\":0,\"id\":118,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":1,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":5,\"menuSort\":999},{\"subCount\":0,\"id\":6,\"menuSort\":999},{\"subCount\":0,\"id\":7,\"menuSort\":999},{\"subCount\":0,\"id\":9,\"menuSort\":999},{\"subCount\":0,\"id\":10,\"menuSort\":999},{\"subCount\":0,\"id\":11,\"menuSort\":999},{\"subCount\":0,\"id\":14,\"menuSort\":999},{\"subCount\":0,\"id\":15,\"menuSort\":999},{\"subCount\":0,\"id\":18,\"menuSort\":999},{\"subCount\":0,\"id\":19,\"menuSort\":999},{\"subCount\":0,\"id\":21,\"menuSort\":999},{\"subCount\":0,\"id\":22,\"menuSort\":999},{\"subCount\":0,\"id\":23,\"menuSort\":999},{\"subCount\":0,\"id\":24,\"menuSort\":999},{\"subCount\":0,\"id\":27,\"menuSort\":999},{\"subCount\":0,\"id\":28,\"menuSort\":999},{\"subCount\":0,\"id\":30,\"menuSort\":999},{\"subCount\":0,\"id\":32,\"menuSort\":999},{\"subCount\":0,\"id\":33,\"menuSort\":999},{\"subCount\":0,\"id\":34,\"menuSort\":999},{\"subCount\":0,\"id\":35,\"menuSort\":999},{\"subCount\":0,\"id\":36,\"menuSort\":999},{\"subCount\":0,\"id\":37,\"menuSort\":999},{\"subCount\":0,\"id\":38,\"menuSort\":999},{\"subCount\":0,\"id\":39,\"menuSort\":999},{\"subCount\":0,\"id\":41,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":46,\"menuSort\":999},{\"subCount\":0,\"id\":48,\"menuSort\":999},{\"subCount\":0,\"id\":49,\"menuSort\":999},{\"subCount\":0,\"id\":50,\"menuSort\":999},{\"subCount\":0,\"id\":52,\"menuSort\":999},{\"subCount\":0,\"id\":53,\"menuSort\":999},{\"subCount\":0,\"id\":54,\"menuSort\":999},{\"subCount\":0,\"id\":56,\"menuSort\":999},{\"subCount\":0,\"id\":57,\"menuSort\":999},{\"subCount\":0,\"id\":58,\"menuSort\":999},{\"subCount\":0,\"id\":60,\"menuSort\":999},{\"subCount\":0,\"id\":61,\"menuSort\":999},{\"subCount\":0,\"id\":62,\"menuSort\":999},{\"subCount\":0,\"id\":64,\"menuSort\":999},{\"subCount\":0,\"id\":65,\"menuSort\":999},{\"subCount\":0,\"id\":66,\"menuSort\":999},{\"subCount\":0,\"id\":73,\"menuSort\":999},{\"subCount\":0,\"id\":74,\"menuSort\":999},{\"subCount\":0,\"id\":75,\"menuSort\":999},{\"subCount\":0,\"id\":77,\"menuSort\":999},{\"subCount\":0,\"id\":78,\"menuSort\":999},{\"subCount\":0,\"id\":79,\"menuSort\":999},{\"subCount\":0,\"id\":80,\"menuSort\":999},{\"subCount\":0,\"id\":82,\"menuSort\":999},{\"subCount\":0,\"id\":83,\"menuSort\":999},{\"subCount\":0,\"id\":90,\"menuSort\":999},{\"subCount\":0,\"id\":92,\"menuSort\":999},{\"subCount\":0,\"id\":93,\"menuSort\":999},{\"subCount\":0,\"id\":94,\"menuSort\":999}]}', '192.168.244.1', 37, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:41:52');
INSERT INTO `sys_log` VALUES (3553, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":3,\"menus\":[{\"subCount\":0,\"id\":35,\"menuSort\":999},{\"subCount\":0,\"id\":37,\"menuSort\":999},{\"subCount\":0,\"id\":39,\"menuSort\":999},{\"subCount\":0,\"id\":41,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":46,\"menuSort\":999},{\"subCount\":0,\"id\":48,\"menuSort\":999},{\"subCount\":0,\"id\":49,\"menuSort\":999},{\"subCount\":0,\"id\":50,\"menuSort\":999},{\"subCount\":0,\"id\":52,\"menuSort\":999},{\"subCount\":0,\"id\":53,\"menuSort\":999},{\"subCount\":0,\"id\":118,\"menuSort\":999},{\"subCount\":0,\"id\":54,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":56,\"menuSort\":999},{\"subCount\":0,\"id\":57,\"menuSort\":999},{\"subCount\":0,\"id\":58,\"menuSort\":999},{\"subCount\":0,\"id\":60,\"menuSort\":999},{\"subCount\":0,\"id\":61,\"menuSort\":999},{\"subCount\":0,\"id\":62,\"menuSort\":999},{\"subCount\":0,\"id\":64,\"menuSort\":999},{\"subCount\":0,\"id\":1,\"menuSort\":999},{\"subCount\":0,\"id\":65,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":66,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":5,\"menuSort\":999},{\"subCount\":0,\"id\":6,\"menuSort\":999},{\"subCount\":0,\"id\":7,\"menuSort\":999},{\"subCount\":0,\"id\":73,\"menuSort\":999},{\"subCount\":0,\"id\":9,\"menuSort\":999},{\"subCount\":0,\"id\":74,\"menuSort\":999},{\"subCount\":0,\"id\":75,\"menuSort\":999},{\"subCount\":0,\"id\":80,\"menuSort\":999},{\"subCount\":0,\"id\":28,\"menuSort\":999},{\"subCount\":0,\"id\":32,\"menuSort\":999}]}', '192.168.244.1', 43, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:42:04');
INSERT INTO `sys_log` VALUES (3554, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"icon\",\"updateTime\":1615275411000,\"title\":\"数据管理\",\"type\":0,\"subCount\":1,\"path\":\"shop\",\"createBy\":\"admin\",\"createTime\":1615275126000,\"iFrame\":false,\"id\":118,\"menuSort\":0}', '192.168.244.1', 13, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:42:50');
INSERT INTO `sys_log` VALUES (3555, '新增菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.create()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"permission\":\"member:add\",\"pid\":119,\"updateTime\":1615276109203,\"title\":\"添加\",\"type\":2,\"subCount\":0,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615276109203,\"iFrame\":false,\"id\":122,\"menuSort\":1}', '192.168.244.1', 7, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:48:29');
INSERT INTO `sys_log` VALUES (3556, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"permission\":\"member:add\",\"pid\":119,\"updateTime\":1615276109000,\"title\":\"会员添加\",\"type\":2,\"subCount\":0,\"createBy\":\"admin\",\"createTime\":1615276109000,\"iFrame\":false,\"id\":122,\"menuSort\":1}', '192.168.244.1', 10, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:48:49');
INSERT INTO `sys_log` VALUES (3557, '新增菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.create()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"permission\":\"member:update\",\"pid\":119,\"updateTime\":1615276157748,\"title\":\"会员修改\",\"type\":2,\"subCount\":0,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615276157748,\"iFrame\":false,\"id\":123,\"menuSort\":2}', '192.168.244.1', 6, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:49:18');
INSERT INTO `sys_log` VALUES (3558, '新增菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.create()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"permission\":\"member:del\",\"pid\":119,\"updateTime\":1615276186686,\"title\":\"会员删除\",\"type\":2,\"subCount\":0,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615276186686,\"iFrame\":false,\"id\":124,\"menuSort\":3}', '192.168.244.1', 7, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:49:47');
INSERT INTO `sys_log` VALUES (3559, '新增菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.create()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"money\",\"permission\":\"rechargerecord:list\",\"pid\":118,\"updateTime\":1615276731982,\"title\":\"充值记录\",\"type\":1,\"subCount\":0,\"path\":\"rechargerecord\",\"component\":\"rechargerecord\",\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1615276731982,\"iFrame\":false,\"id\":129,\"componentName\":\"RechargeRecord\",\"menuSort\":999}', '192.168.244.1', 14, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 15:58:52');
INSERT INTO `sys_log` VALUES (3560, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"money\",\"permission\":\"rechargerecord:list\",\"pid\":118,\"updateTime\":1615276732000,\"title\":\"充值记录\",\"type\":1,\"subCount\":0,\"path\":\"rechargerecord\",\"component\":\"bigtime/rechargerecord/index\",\"createBy\":\"admin\",\"createTime\":1615276732000,\"iFrame\":false,\"id\":129,\"componentName\":\"RechargeRecord\",\"menuSort\":999}', '192.168.244.1', 11, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 16:00:53');
INSERT INTO `sys_log` VALUES (3561, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"money\",\"permission\":\"rechargerecord:list\",\"pid\":118,\"updateTime\":1615276853000,\"title\":\"充值记录\",\"type\":1,\"subCount\":0,\"path\":\"rechargerecord\",\"component\":\"bigtime/rechargerecord/index\",\"createBy\":\"admin\",\"createTime\":1615276732000,\"iFrame\":false,\"id\":129,\"componentName\":\"RechargeRecord\",\"menuSort\":1}', '192.168.244.1', 14, 'admin', '内网IP', 'Chrome 8', NULL, '2021-03-09 16:50:23');
INSERT INTO `sys_log` VALUES (3562, '修改角色', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.update()', '{\"level\":4,\"description\":\"-\",\"updateTime\":1615020120000,\"dataScope\":\"本级\",\"createTime\":1542949746000,\"name\":\"职员\",\"id\":2,\"menus\":[{\"cache\":false,\"hidden\":false,\"icon\":\"markdown\",\"pid\":10,\"title\":\"Markdown\",\"type\":1,\"subCount\":0,\"path\":\"markdown\",\"component\":\"components/MarkDown\",\"createTime\":1552024004000,\"iFrame\":false,\"id\":33,\"componentName\":\"Markdown\",\"menuSort\":53},{\"cache\":false,\"hidden\":false,\"icon\":\"system\",\"title\":\"系统管理\",\"type\":0,\"subCount\":7,\"path\":\"system\",\"createTime\":1545117089000,\"iFrame\":false,\"id\":1,\"menuSort\":1},{\"cache\":false,\"hidden\":false,\"icon\":\"dev\",\"pid\":10,\"title\":\"Yaml编辑器\",\"type\":1,\"subCount\":0,\"path\":\"yaml\",\"component\":\"components/YamlEdit\",\"createTime\":1552031380000,\"iFrame\":false,\"id\":34,\"componentName\":\"YamlEdit\",\"menuSort\":54},{\"cache\":false,\"hidden\":false,\"icon\":\"peoples\",\"permission\":\"user:list\",\"pid\":1,\"title\":\"用户管理\",\"type\":1,\"subCount\":3,\"path\":\"user\",\"component\":\"system/user/index\",\"createTime\":1545117284000,\"iFrame\":false,\"id\":2,\"componentName\":\"User\",\"menuSort\":2},{\"cache\":false,\"hidden\":false,\"icon\":\"sys-tools\",\"title\":\"系统工具\",\"type\":0,\"subCount\":7,\"path\":\"sys-tools\",\"component\":\"\",\"createTime\":1553828255000,\"iFrame\":false,\"id\":36,\"menuSort\":30},{\"cache\":false,\"hidden\":false,\"icon\":\"monitor\",\"title\":\"系统监控\",\"type\":0,\"subCount\":5,\"path\":\"monitor\",\"createTime\":1545117468000,\"iFrame\":false,\"id\":6,\"menuSort\":10},{\"cache\":true,\"hidden\":false,\"icon\":\"log\",\"pid\":6,\"updateTime\":1591420317000,\"title\":\"操作日志\",\"type\":1,\"subCount\":0,\"path\":\"logs\",\"component\":\"monitor/log/index\",\"createTime\":1545117506000,\"iFrame\":false,\"id\":7,\"componentName\":\"Log\",\"menuSort\":11},{\"cache\":false,\"hidden\":false,\"icon\":\"sqlMonitor\",\"pid\":6,\"title\":\"SQL监控\",\"type\":1,\"subCount\":0,\"path\":\"druid\",\"component\":\"monitor/sql/index\",\"createTime\":1545117574000,\"iFrame\":false,\"id\":9,\"componentName\":\"Sql\",\"menuSort\":18},{\"cache\":false,\"hidden\":false,\"icon\":\"zujian\",\"title\":\"组件管理\",\"type\":0,\"subCount\":5,\"path\":\"components\",\"createTime\":1545197896000,\"iFrame\":false,\"id\":10,\"menuSort\":50},{\"cache\":false,\"hidden\":false,\"icon\":\"icon\",\"pid\":10,\"title\":\"图标库\",\"type\":1,\"subCount\":0,\"path\":\"icon\",\"component\":\"components/icons/index\",\"createTime\":1545197929000,\"iFrame\":false,\"id\":11,\"componentName\":\"Icons\",\"menuSort\":51},{\"cache\":false,\"hidden\":false,\"icon\":\"email\",\"pid\":36,\"title\":\"邮件工具\",\"type\":1,\"subCount\":0,\"path\":\"email\",\"component\":\"tools/email/index\",\"createTime\":1545876789000,\"iFrame\":false,\"id\":14,\"componentName\":\"Email\",\"menuSort\":35},{\"cache\":false,\"hidden\":false,\"icon\":\"fwb\",\"pid\":10,\"title\":\"富文本\",\"type\":1,\"subCount\":0,\"path\":\"tinymce\",\"component\":\"components/Editor\",\"createTime\":1545883105000,\"iFrame\":false,\"id\":15,\"componentName\":\"Editor\",\"menuSort\":52},{\"cache\":false,\"hidden\":false,\"icon\":\"codeConsole\",\"permission\":\"monitor:list\",\"pid\":6,\"updateTime\":1588587650000,\"title\":\"服务监控\",\"type\":1,\"subCount\":0,\"path\":\"server\",\"component\":\"monitor/server/index\",\"createTime\":1573103199000,\"iFrame\":false,\"id\":80,\"componentName\":\"ServerMonitor\",\"menuSort\":14},{\"cache\":true,\"hidden\":true,\"icon\":\"dev\",\"permission\":\"\",\"pid\":36,\"title\":\"生成配置\",\"type\":1,\"subCount\":0,\"path\":\"generator/config/:tableName\",\"component\":\"generator/config\",\"createTime\":1573992536000,\"iFrame\":false,\"id\":82,\"componentName\":\"GeneratorConfig\",\"menuSort\":33},{\"cache\":false,\"hidden\":false,\"icon\":\"alipay\",\"pid\":36,\"title\":\"支付宝工具\",\"type\":1,\"subCount\":0,\"path\":\"aliPay\",\"component\":\"tools/aliPay/index\",\"createTime\":1546239158000,\"iFrame\":false,\"id\":19,\"componentName\":\"AliPay\",\"menuSort\":37},{\"cache\":true,\"hidden\":false,\"icon\":\"chart\",\"permission\":\"\",\"pid\":10,\"title\":\"图表库\",\"type\":1,\"subCount\":0,\"path\":\"echarts\",\"component\":\"components/Echarts\",\"createTime\":1574298272000,\"iFrame\":false,\"id\":83,\"componentName\":\"Echarts\",\"menuSort\":50},{\"cache\":true,\"hidden\":true,\"icon\":\"java\",\"pid\":36,\"title\":\"生成预览\",\"type\":1,\"subCount\":0,\"path\":\"generator/preview/:tableName\",\"component\":\"generator/preview\",\"createTime\":1574751276000,\"iFrame\":false,\"id\":116,\"componentName\":\"Preview\",\"menuSort\":999},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"updateTime\":1592731655000,\"title\":\"多级菜单\",\"type\":0,\"subCount\":2,\"path\":\"nested\",\"component\":\"\",\"createTime\":1546590123000,\"iFrame\":false,\"id\":21,\"menuSort\":900},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"pid\":21,\"updateTime\":1592731640000,\"title\":\"二级菜单1\",\"type\":0,\"subCount\":2,\"path\":\"menu1\",\"component\":\"\",\"createTime\":1546590209000,\"iFrame\":false,\"id\":22,\"menuSort\":999},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"pid\":21,\"title\":\"二级菜单2\",\"type\":1,\"subCount\":0,\"path\":\"menu2\",\"component\":\"nested/menu2/index\",\"createTime\":1546590237000,\"iFrame\":false,\"id\":23,\"menuSort\":999},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"pid\":22,\"title\":\"三级菜单1\",\"type\":1,\"subCount\":0,\"path\":\"menu1-1\",\"component\":\"nested/menu1/menu1-1\",\"createTime\":1546590288000,\"iFrame\":false,\"id\":24,\"componentName\":\"Test\",\"menuSort\":999},{\"cache\":false,\"hidden\":false,\"icon\":\"menu\",\"pid\":22,\"title\":\"三级菜单2\",\"type\":1,\"subCount\":0,\"path\":\"menu1-2\",\"component\":\"nested/menu1/menu1-2\",\"createTime\":1546853252000,\"iFrame\":false,\"id\":27,\"menuSort\":999},{\"cache\":true,\"hidden\":false,\"icon\":\"dev\",\"pid\":36,\"title\":\"代码生成\",\"type\":1,\"subCount\":0,\"path\":\"generator\",\"component\":\"generator/index\",\"createTime\":1547192755000,\"iFrame\":false,\"id\":30,\"componentName\":\"GeneratorIndex\",\"menuSort\":32},{\"cache\":false,\"hidden\":false,\"icon\":\"error\",\"pid\":6,\"title\":\"异常日志\",\"type\":1,\"subCount\":0,\"path\":\"errorLog\",\"component\":\"monitor/log/errorLog\",\"createTime\":1547358543000,\"iFrame\":false,\"id\":32,\"componentName\":\"ErrorLog\",\"menuSort\":12}],\"depts\":[]}', '192.168.244.1', 67, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 09:43:55');
INSERT INTO `sys_log` VALUES (3563, '修改岗位', 'INFO', 'me.zhengjie.modules.system.rest.JobController.update()', '{\"updateTime\":1588649623000,\"enabled\":true,\"jobSort\":2,\"createTime\":1554010770000,\"name\":\"老板\",\"id\":11}', '192.168.244.1', 18, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 09:44:16');
INSERT INTO `sys_log` VALUES (3564, '修改岗位', 'INFO', 'me.zhengjie.modules.system.rest.JobController.update()', '{\"updateTime\":1619660656000,\"enabled\":true,\"jobSort\":2,\"createTime\":1554010770000,\"name\":\"全栈开发\",\"id\":11}', '192.168.244.1', 14, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 09:44:32');
INSERT INTO `sys_log` VALUES (3565, '新增岗位', 'INFO', 'me.zhengjie.modules.system.rest.JobController.create()', '{\"updateTime\":1619660688762,\"enabled\":true,\"jobSort\":11,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1619660688762,\"name\":\"老板\",\"id\":14}', '192.168.244.1', 10, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 09:44:49');
INSERT INTO `sys_log` VALUES (3566, '新增岗位', 'INFO', 'me.zhengjie.modules.system.rest.JobController.create()', '{\"updateTime\":1619660697321,\"enabled\":true,\"jobSort\":12,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1619660697321,\"name\":\"店铺经理\",\"id\":15}', '192.168.244.1', 8, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 09:44:57');
INSERT INTO `sys_log` VALUES (3567, '新增岗位', 'INFO', 'me.zhengjie.modules.system.rest.JobController.create()', '{\"updateTime\":1619660704351,\"enabled\":true,\"jobSort\":13,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1619660704351,\"name\":\"职员\",\"id\":16}', '192.168.244.1', 8, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 09:45:04');
INSERT INTO `sys_log` VALUES (3568, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":3,\"menus\":[{\"subCount\":0,\"id\":37,\"menuSort\":999},{\"subCount\":0,\"id\":39,\"menuSort\":999},{\"subCount\":0,\"id\":41,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":46,\"menuSort\":999},{\"subCount\":0,\"id\":48,\"menuSort\":999},{\"subCount\":0,\"id\":49,\"menuSort\":999},{\"subCount\":0,\"id\":50,\"menuSort\":999},{\"subCount\":0,\"id\":118,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":60,\"menuSort\":999},{\"subCount\":0,\"id\":124,\"menuSort\":999},{\"subCount\":0,\"id\":61,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":62,\"menuSort\":999},{\"subCount\":0,\"id\":126,\"menuSort\":999},{\"subCount\":0,\"id\":127,\"menuSort\":999},{\"subCount\":0,\"id\":64,\"menuSort\":999},{\"subCount\":0,\"id\":128,\"menuSort\":999},{\"subCount\":0,\"id\":1,\"menuSort\":999},{\"subCount\":0,\"id\":65,\"menuSort\":999},{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":66,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":132,\"menuSort\":999},{\"subCount\":0,\"id\":133,\"menuSort\":999},{\"subCount\":0,\"id\":6,\"menuSort\":999},{\"subCount\":0,\"id\":134,\"menuSort\":999},{\"subCount\":0,\"id\":7,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":136,\"menuSort\":999},{\"subCount\":0,\"id\":9,\"menuSort\":999},{\"subCount\":0,\"id\":137,\"menuSort\":999},{\"subCount\":0,\"id\":138,\"menuSort\":999},{\"subCount\":0,\"id\":80,\"menuSort\":999},{\"subCount\":0,\"id\":32,\"menuSort\":999}]}', '192.168.244.1', 83, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 09:49:13');
INSERT INTO `sys_log` VALUES (3569, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":3,\"menus\":[{\"subCount\":0,\"id\":37,\"menuSort\":999},{\"subCount\":0,\"id\":39,\"menuSort\":999},{\"subCount\":0,\"id\":41,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":46,\"menuSort\":999},{\"subCount\":0,\"id\":48,\"menuSort\":999},{\"subCount\":0,\"id\":49,\"menuSort\":999},{\"subCount\":0,\"id\":50,\"menuSort\":999},{\"subCount\":0,\"id\":118,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":60,\"menuSort\":999},{\"subCount\":0,\"id\":124,\"menuSort\":999},{\"subCount\":0,\"id\":61,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":62,\"menuSort\":999},{\"subCount\":0,\"id\":126,\"menuSort\":999},{\"subCount\":0,\"id\":127,\"menuSort\":999},{\"subCount\":0,\"id\":64,\"menuSort\":999},{\"subCount\":0,\"id\":128,\"menuSort\":999},{\"subCount\":0,\"id\":1,\"menuSort\":999},{\"subCount\":0,\"id\":65,\"menuSort\":999},{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":66,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":132,\"menuSort\":999},{\"subCount\":0,\"id\":133,\"menuSort\":999},{\"subCount\":0,\"id\":6,\"menuSort\":999},{\"subCount\":0,\"id\":134,\"menuSort\":999},{\"subCount\":0,\"id\":7,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":136,\"menuSort\":999},{\"subCount\":0,\"id\":9,\"menuSort\":999},{\"subCount\":0,\"id\":137,\"menuSort\":999},{\"subCount\":0,\"id\":138,\"menuSort\":999},{\"subCount\":0,\"id\":80,\"menuSort\":999},{\"subCount\":0,\"id\":32,\"menuSort\":999}]}', '192.168.244.1', 21, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 09:49:16');
INSERT INTO `sys_log` VALUES (3570, '新增菜单', 'ERROR', 'me.zhengjie.modules.system.rest.MenuController.create()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"permission\":\"add\",\"pid\":129,\"title\":\"添加充值记录\",\"type\":2,\"subCount\":0,\"iFrame\":false,\"menuSort\":2}', '192.168.244.1', 6, 'admin', '内网IP', 'Chrome 8', 'me.zhengjie.exception.EntityExistException: Menu with title 添加充值记录 existed\r\n	at me.zhengjie.modules.system.service.impl.MenuServiceImpl.create(MenuServiceImpl.java:111)\r\n	at me.zhengjie.modules.system.service.impl.MenuServiceImpl$$FastClassBySpringCGLIB$$a0cfbc77.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:771)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.transaction.interceptor.TransactionAspectSupport.invokeWithinTransaction(TransactionAspectSupport.java:367)\r\n	at org.springframework.transaction.interceptor.TransactionInterceptor.invoke(TransactionInterceptor.java:118)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:691)\r\n	at me.zhengjie.modules.system.service.impl.MenuServiceImpl$$EnhancerBySpringCGLIB$$646b4ac7.create(<generated>)\r\n	at me.zhengjie.modules.system.rest.MenuController.create(MenuController.java:120)\r\n	at me.zhengjie.modules.system.rest.MenuController$$FastClassBySpringCGLIB$$158b4bdf.invoke(<generated>)\r\n	at org.springframework.cglib.proxy.MethodProxy.invoke(MethodProxy.java:218)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.invokeJoinpoint(CglibAopProxy.java:771)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:163)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.aspectj.AspectJAfterThrowingAdvice.invoke(AspectJAfterThrowingAdvice.java:62)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:175)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.aspectj.MethodInvocationProceedingJoinPoint.proceed(MethodInvocationProceedingJoinPoint.java:88)\r\n	at me.zhengjie.aspect.LogAspect.logAround(LogAspect.java:68)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n	at java.lang.reflect.Method.invoke(Unknown Source)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethodWithGivenArgs(AbstractAspectJAdvice.java:644)\r\n	at org.springframework.aop.aspectj.AbstractAspectJAdvice.invokeAdviceMethod(AbstractAspectJAdvice.java:633)\r\n	at org.springframework.aop.aspectj.AspectJAroundAdvice.invoke(AspectJAroundAdvice.java:70)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:175)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.security.access.intercept.aopalliance.MethodSecurityInterceptor.invoke(MethodSecurityInterceptor.java:69)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.interceptor.ExposeInvocationInterceptor.invoke(ExposeInvocationInterceptor.java:95)\r\n	at org.springframework.aop.framework.ReflectiveMethodInvocation.proceed(ReflectiveMethodInvocation.java:186)\r\n	at org.springframework.aop.framework.CglibAopProxy$CglibMethodInvocation.proceed(CglibAopProxy.java:749)\r\n	at org.springframework.aop.framework.CglibAopProxy$DynamicAdvisedInterceptor.intercept(CglibAopProxy.java:691)\r\n	at me.zhengjie.modules.system.rest.MenuController$$EnhancerBySpringCGLIB$$b705f5b7.create(<generated>)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)\r\n	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)\r\n	at java.lang.reflect.Method.invoke(Unknown Source)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.doInvoke(InvocableHandlerMethod.java:190)\r\n	at org.springframework.web.method.support.InvocableHandlerMethod.invokeForRequest(InvocableHandlerMethod.java:138)\r\n	at org.springframework.web.servlet.mvc.method.annotation.ServletInvocableHandlerMethod.invokeAndHandle(ServletInvocableHandlerMethod.java:105)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.invokeHandlerMethod(RequestMappingHandlerAdapter.java:878)\r\n	at org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter.handleInternal(RequestMappingHandlerAdapter.java:792)\r\n	at org.springframework.web.servlet.mvc.method.AbstractHandlerMethodAdapter.handle(AbstractHandlerMethodAdapter.java:87)\r\n	at org.springframework.web.servlet.DispatcherServlet.doDispatch(DispatcherServlet.java:1040)\r\n	at org.springframework.web.servlet.DispatcherServlet.doService(DispatcherServlet.java:943)\r\n	at org.springframework.web.servlet.FrameworkServlet.processRequest(FrameworkServlet.java:1006)\r\n	at org.springframework.web.servlet.FrameworkServlet.doPost(FrameworkServlet.java:909)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:652)\r\n	at org.springframework.web.servlet.FrameworkServlet.service(FrameworkServlet.java:883)\r\n	at javax.servlet.http.HttpServlet.service(HttpServlet.java:733)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:231)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.tomcat.websocket.server.WsFilter.doFilter(WsFilter.java:53)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:113)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at com.alibaba.druid.support.http.WebStatFilter.doFilter(WebStatFilter.java:124)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:320)\r\n	at org.springframework.security.web.access.intercept.FilterSecurityInterceptor.invoke(FilterSecurityInterceptor.java:126)\r\n	at org.springframework.security.web.access.intercept.FilterSecurityInterceptor.doFilter(FilterSecurityInterceptor.java:90)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.access.ExceptionTranslationFilter.doFilter(ExceptionTranslationFilter.java:118)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.session.SessionManagementFilter.doFilter(SessionManagementFilter.java:137)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.authentication.AnonymousAuthenticationFilter.doFilter(AnonymousAuthenticationFilter.java:111)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter.doFilter(SecurityContextHolderAwareRequestFilter.java:158)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.savedrequest.RequestCacheAwareFilter.doFilter(RequestCacheAwareFilter.java:63)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at me.zhengjie.modules.security.security.TokenFilter.doFilter(TokenFilter.java:90)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.web.filter.CorsFilter.doFilterInternal(CorsFilter.java:92)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.authentication.logout.LogoutFilter.doFilter(LogoutFilter.java:116)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.header.HeaderWriterFilter.doHeadersAfter(HeaderWriterFilter.java:92)\r\n	at org.springframework.security.web.header.HeaderWriterFilter.doFilterInternal(HeaderWriterFilter.java:77)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.context.SecurityContextPersistenceFilter.doFilter(SecurityContextPersistenceFilter.java:105)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.context.request.async.WebAsyncManagerIntegrationFilter.doFilterInternal(WebAsyncManagerIntegrationFilter.java:56)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.springframework.security.web.FilterChainProxy$VirtualFilterChain.doFilter(FilterChainProxy.java:334)\r\n	at org.springframework.security.web.FilterChainProxy.doFilterInternal(FilterChainProxy.java:215)\r\n	at org.springframework.security.web.FilterChainProxy.doFilter(FilterChainProxy.java:178)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.invokeDelegate(DelegatingFilterProxy.java:358)\r\n	at org.springframework.web.filter.DelegatingFilterProxy.doFilter(DelegatingFilterProxy.java:271)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.springframework.web.filter.CharacterEncodingFilter.doFilterInternal(CharacterEncodingFilter.java:201)\r\n	at org.springframework.web.filter.OncePerRequestFilter.doFilter(OncePerRequestFilter.java:119)\r\n	at org.apache.catalina.core.ApplicationFilterChain.internalDoFilter(ApplicationFilterChain.java:193)\r\n	at org.apache.catalina.core.ApplicationFilterChain.doFilter(ApplicationFilterChain.java:166)\r\n	at org.apache.catalina.core.StandardWrapperValve.invoke(StandardWrapperValve.java:202)\r\n	at org.apache.catalina.core.StandardContextValve.invoke(StandardContextValve.java:97)\r\n	at org.apache.catalina.authenticator.AuthenticatorBase.invoke(AuthenticatorBase.java:541)\r\n	at org.apache.catalina.core.StandardHostValve.invoke(StandardHostValve.java:143)\r\n	at org.apache.catalina.valves.ErrorReportValve.invoke(ErrorReportValve.java:92)\r\n	at org.apache.catalina.core.StandardEngineValve.invoke(StandardEngineValve.java:78)\r\n	at org.apache.catalina.connector.CoyoteAdapter.service(CoyoteAdapter.java:343)\r\n	at org.apache.coyote.http11.Http11Processor.service(Http11Processor.java:374)\r\n	at org.apache.coyote.AbstractProcessorLight.process(AbstractProcessorLight.java:65)\r\n	at org.apache.coyote.AbstractProtocol$ConnectionHandler.process(AbstractProtocol.java:868)\r\n	at org.apache.tomcat.util.net.NioEndpoint$SocketProcessor.doRun(NioEndpoint.java:1590)\r\n	at org.apache.tomcat.util.net.SocketProcessorBase.run(SocketProcessorBase.java:49)\r\n	at java.util.concurrent.ThreadPoolExecutor.runWorker(Unknown Source)\r\n	at java.util.concurrent.ThreadPoolExecutor$Worker.run(Unknown Source)\r\n	at org.apache.tomcat.util.threads.TaskThread$WrappingRunnable.run(TaskThread.java:61)\r\n	at java.lang.Thread.run(Unknown Source)\r\n', '2021-04-29 09:57:26');
INSERT INTO `sys_log` VALUES (3571, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":1,\"menus\":[{\"subCount\":0,\"id\":1,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":5,\"menuSort\":999},{\"subCount\":0,\"id\":6,\"menuSort\":999},{\"subCount\":0,\"id\":7,\"menuSort\":999},{\"subCount\":0,\"id\":9,\"menuSort\":999},{\"subCount\":0,\"id\":10,\"menuSort\":999},{\"subCount\":0,\"id\":11,\"menuSort\":999},{\"subCount\":0,\"id\":14,\"menuSort\":999},{\"subCount\":0,\"id\":15,\"menuSort\":999},{\"subCount\":0,\"id\":18,\"menuSort\":999},{\"subCount\":0,\"id\":19,\"menuSort\":999},{\"subCount\":0,\"id\":21,\"menuSort\":999},{\"subCount\":0,\"id\":22,\"menuSort\":999},{\"subCount\":0,\"id\":23,\"menuSort\":999},{\"subCount\":0,\"id\":24,\"menuSort\":999},{\"subCount\":0,\"id\":27,\"menuSort\":999},{\"subCount\":0,\"id\":28,\"menuSort\":999},{\"subCount\":0,\"id\":30,\"menuSort\":999},{\"subCount\":0,\"id\":32,\"menuSort\":999},{\"subCount\":0,\"id\":33,\"menuSort\":999},{\"subCount\":0,\"id\":34,\"menuSort\":999},{\"subCount\":0,\"id\":35,\"menuSort\":999},{\"subCount\":0,\"id\":36,\"menuSort\":999},{\"subCount\":0,\"id\":37,\"menuSort\":999},{\"subCount\":0,\"id\":38,\"menuSort\":999},{\"subCount\":0,\"id\":39,\"menuSort\":999},{\"subCount\":0,\"id\":41,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":46,\"menuSort\":999},{\"subCount\":0,\"id\":48,\"menuSort\":999},{\"subCount\":0,\"id\":49,\"menuSort\":999},{\"subCount\":0,\"id\":50,\"menuSort\":999},{\"subCount\":0,\"id\":52,\"menuSort\":999},{\"subCount\":0,\"id\":53,\"menuSort\":999},{\"subCount\":0,\"id\":54,\"menuSort\":999},{\"subCount\":0,\"id\":56,\"menuSort\":999},{\"subCount\":0,\"id\":57,\"menuSort\":999},{\"subCount\":0,\"id\":58,\"menuSort\":999},{\"subCount\":0,\"id\":60,\"menuSort\":999},{\"subCount\":0,\"id\":61,\"menuSort\":999},{\"subCount\":0,\"id\":62,\"menuSort\":999},{\"subCount\":0,\"id\":64,\"menuSort\":999},{\"subCount\":0,\"id\":65,\"menuSort\":999},{\"subCount\":0,\"id\":66,\"menuSort\":999},{\"subCount\":0,\"id\":73,\"menuSort\":999},{\"subCount\":0,\"id\":74,\"menuSort\":999},{\"subCount\":0,\"id\":75,\"menuSort\":999},{\"subCount\":0,\"id\":77,\"menuSort\":999},{\"subCount\":0,\"id\":78,\"menuSort\":999},{\"subCount\":0,\"id\":79,\"menuSort\":999},{\"subCount\":0,\"id\":80,\"menuSort\":999},{\"subCount\":0,\"id\":82,\"menuSort\":999},{\"subCount\":0,\"id\":83,\"menuSort\":999},{\"subCount\":0,\"id\":90,\"menuSort\":999},{\"subCount\":0,\"id\":92,\"menuSort\":999},{\"subCount\":0,\"id\":93,\"menuSort\":999},{\"subCount\":0,\"id\":94,\"menuSort\":999},{\"subCount\":0,\"id\":97,\"menuSort\":999},{\"subCount\":0,\"id\":98,\"menuSort\":999},{\"subCount\":0,\"id\":102,\"menuSort\":999},{\"subCount\":0,\"id\":103,\"menuSort\":999},{\"subCount\":0,\"id\":104,\"menuSort\":999},{\"subCount\":0,\"id\":105,\"menuSort\":999},{\"subCount\":0,\"id\":106,\"menuSort\":999},{\"subCount\":0,\"id\":107,\"menuSort\":999},{\"subCount\":0,\"id\":108,\"menuSort\":999},{\"subCount\":0,\"id\":109,\"menuSort\":999},{\"subCount\":0,\"id\":110,\"menuSort\":999},{\"subCount\":0,\"id\":111,\"menuSort\":999},{\"subCount\":0,\"id\":112,\"menuSort\":999},{\"subCount\":0,\"id\":113,\"menuSort\":999},{\"subCount\":0,\"id\":114,\"menuSort\":999},{\"subCount\":0,\"id\":116,\"menuSort\":999},{\"subCount\":0,\"id\":118,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":124,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":126,\"menuSort\":999},{\"subCount\":0,\"id\":127,\"menuSort\":999},{\"subCount\":0,\"id\":128,\"menuSort\":999},{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":132,\"menuSort\":999},{\"subCount\":0,\"id\":133,\"menuSort\":999},{\"subCount\":0,\"id\":134,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":136,\"menuSort\":999},{\"subCount\":0,\"id\":137,\"menuSort\":999},{\"subCount\":0,\"id\":138,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":140,\"menuSort\":999},{\"subCount\":0,\"id\":141,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999},{\"subCount\":0,\"id\":143,\"menuSort\":999},{\"subCount\":0,\"id\":144,\"menuSort\":999}]}', '192.168.3.114', 53, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:02:09');
INSERT INTO `sys_log` VALUES (3572, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"icon\":\"money\",\"permission\":\"rechargerecord:list\",\"pid\":118,\"updateTime\":1615279823000,\"title\":\"充值记录\",\"type\":1,\"subCount\":0,\"path\":\"rechargerecord\",\"component\":\"bigtime/rechargerecord/index\",\"createBy\":\"admin\",\"createTime\":1615276732000,\"iFrame\":false,\"id\":129,\"componentName\":\"RechargeRecord\",\"menuSort\":1}', '192.168.3.114', 49, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:02:56');
INSERT INTO `sys_log` VALUES (3573, '新增菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.create()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"permission\":\"rechargerecord:add\",\"pid\":129,\"updateTime\":1619661907532,\"title\":\"添加充值记录1\",\"type\":2,\"subCount\":0,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1619661907532,\"iFrame\":false,\"id\":145,\"menuSort\":1}', '192.168.3.114', 18, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:05:08');
INSERT INTO `sys_log` VALUES (3574, '删除菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.delete()', '[145]', '192.168.3.114', 22, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:05:51');
INSERT INTO `sys_log` VALUES (3575, '新增菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.create()', '{\"cache\":false,\"hidden\":false,\"roles\":[],\"permission\":\"1\",\"pid\":130,\"updateTime\":1619661975076,\"title\":\"1\",\"type\":2,\"subCount\":0,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1619661975076,\"iFrame\":false,\"id\":146,\"menuSort\":999}', '192.168.3.114', 11, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:06:15');
INSERT INTO `sys_log` VALUES (3576, '删除菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.delete()', '[146]', '192.168.3.114', 16, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:06:23');
INSERT INTO `sys_log` VALUES (3577, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":3,\"menus\":[{\"subCount\":0,\"id\":37,\"menuSort\":999},{\"subCount\":0,\"id\":39,\"menuSort\":999},{\"subCount\":0,\"id\":41,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":46,\"menuSort\":999},{\"subCount\":0,\"id\":48,\"menuSort\":999},{\"subCount\":0,\"id\":49,\"menuSort\":999},{\"subCount\":0,\"id\":50,\"menuSort\":999},{\"subCount\":0,\"id\":118,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":60,\"menuSort\":999},{\"subCount\":0,\"id\":124,\"menuSort\":999},{\"subCount\":0,\"id\":61,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":62,\"menuSort\":999},{\"subCount\":0,\"id\":126,\"menuSort\":999},{\"subCount\":0,\"id\":127,\"menuSort\":999},{\"subCount\":0,\"id\":64,\"menuSort\":999},{\"subCount\":0,\"id\":128,\"menuSort\":999},{\"subCount\":0,\"id\":1,\"menuSort\":999},{\"subCount\":0,\"id\":65,\"menuSort\":999},{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":66,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":132,\"menuSort\":999},{\"subCount\":0,\"id\":133,\"menuSort\":999},{\"subCount\":0,\"id\":6,\"menuSort\":999},{\"subCount\":0,\"id\":134,\"menuSort\":999},{\"subCount\":0,\"id\":7,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":136,\"menuSort\":999},{\"subCount\":0,\"id\":9,\"menuSort\":999},{\"subCount\":0,\"id\":137,\"menuSort\":999},{\"subCount\":0,\"id\":138,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":140,\"menuSort\":999},{\"subCount\":0,\"id\":141,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999},{\"subCount\":0,\"id\":143,\"menuSort\":999},{\"subCount\":0,\"id\":80,\"menuSort\":999},{\"subCount\":0,\"id\":144,\"menuSort\":999},{\"subCount\":0,\"id\":32,\"menuSort\":999}]}', '192.168.3.114', 34, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:06:39');
INSERT INTO `sys_log` VALUES (3578, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":4,\"menus\":[{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999}]}', '192.168.3.114', 37, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:08:31');
INSERT INTO `sys_log` VALUES (3579, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":2,\"menus\":[{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":143,\"menuSort\":999}]}', '192.168.3.114', 48, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:09:17');
INSERT INTO `sys_log` VALUES (3580, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":4,\"menus\":[{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":136,\"menuSort\":999},{\"subCount\":0,\"id\":137,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999}]}', '192.168.3.114', 20, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:09:24');
INSERT INTO `sys_log` VALUES (3581, '修改部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.update()', '{\"updateTime\":1591589336000,\"enabled\":true,\"deptSort\":0,\"subCount\":2,\"createBy\":\"admin\",\"createTime\":1553483090000,\"name\":\"河南\",\"id\":7}', '192.168.3.114', 23, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:09:51');
INSERT INTO `sys_log` VALUES (3582, '修改部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.update()', '{\"pid\":7,\"updateTime\":1596350927000,\"enabled\":true,\"deptSort\":3,\"subCount\":1,\"createBy\":\"admin\",\"createTime\":1553476532000,\"name\":\"郑州\",\"id\":2}', '192.168.3.114', 17, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:10:00');
INSERT INTO `sys_log` VALUES (3583, '修改部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.update()', '{\"pid\":7,\"updateTime\":1589696847000,\"enabled\":true,\"deptSort\":4,\"subCount\":0,\"createBy\":\"admin\",\"createTime\":1553476844000,\"name\":\"金水区\",\"id\":5}', '192.168.3.114', 25, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:10:08');
INSERT INTO `sys_log` VALUES (3584, '删除部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.delete()', '[8]', '192.168.3.114', 28, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:10:13');
INSERT INTO `sys_log` VALUES (3585, '删除部门', 'INFO', 'me.zhengjie.modules.system.rest.DeptController.delete()', '[18]', '192.168.3.114', 20, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-29 10:10:15');
INSERT INTO `sys_log` VALUES (3586, '查询member', 'INFO', 'com.bigtime.rest.MemberController.query()', '', '192.168.244.1', 40, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:30');
INSERT INTO `sys_log` VALUES (3587, '查询combo', 'INFO', 'com.bigtime.rest.ComboController.query()', '', '192.168.244.1', 15, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:32');
INSERT INTO `sys_log` VALUES (3588, '查询rechargerecord', 'INFO', 'com.bigtime.rest.RechargeRecordController.query()', '', '192.168.244.1', 17, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:41');
INSERT INTO `sys_log` VALUES (3589, '查询playercashlog', 'INFO', 'com.bigtime.rest.PlayerCashlogController.query()', '', '192.168.244.1', 13, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:42');
INSERT INTO `sys_log` VALUES (3590, '查询combo', 'INFO', 'com.bigtime.rest.ComboController.query()', '', '192.168.244.1', 8, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:45');
INSERT INTO `sys_log` VALUES (3591, '查询member', 'INFO', 'com.bigtime.rest.MemberController.query()', '', '192.168.244.1', 8, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:48');
INSERT INTO `sys_log` VALUES (3592, '查询combo', 'INFO', 'com.bigtime.rest.ComboController.query()', '', '192.168.244.1', 10, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:55');
INSERT INTO `sys_log` VALUES (3593, '查询rechargerecord', 'INFO', 'com.bigtime.rest.RechargeRecordController.query()', '', '192.168.244.1', 13, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:56');
INSERT INTO `sys_log` VALUES (3594, '查询playercashlog', 'INFO', 'com.bigtime.rest.PlayerCashlogController.query()', '', '192.168.244.1', 6, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:57');
INSERT INTO `sys_log` VALUES (3595, '查询shop', 'INFO', 'com.bigtime.rest.ShopController.query()', '', '192.168.244.1', 22, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:58');
INSERT INTO `sys_log` VALUES (3596, '查询toy', 'INFO', 'com.bigtime.rest.ToyController.query()', '', '192.168.244.1', 19, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:58');
INSERT INTO `sys_log` VALUES (3597, '查询member', 'INFO', 'com.bigtime.rest.MemberController.query()', '', '192.168.244.1', 7, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:36:59');
INSERT INTO `sys_log` VALUES (3598, '新增岗位', 'INFO', 'me.zhengjie.modules.system.rest.JobController.create()', '{\"updateTime\":1619743117561,\"enabled\":true,\"jobSort\":999,\"createBy\":\"admin\",\"updateBy\":\"admin\",\"createTime\":1619743117561,\"name\":\"会员\",\"id\":17}', '192.168.244.1', 52, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:38:38');
INSERT INTO `sys_log` VALUES (3599, '查询member', 'INFO', 'com.bigtime.rest.MemberController.query()', '', '192.168.244.1', 10, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 08:38:41');
INSERT INTO `sys_log` VALUES (3600, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":2,\"menus\":[{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999}]}', '192.168.244.1', 106, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:11:22');
INSERT INTO `sys_log` VALUES (3601, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":2,\"menus\":[{\"subCount\":0,\"id\":145,\"menuSort\":999},{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":146,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999}]}', '192.168.244.1', 47, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:18:19');
INSERT INTO `sys_log` VALUES (3602, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":4,\"menus\":[{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":136,\"menuSort\":999},{\"subCount\":0,\"id\":137,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999},{\"subCount\":0,\"id\":145,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999}]}', '192.168.244.1', 44, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:19:02');
INSERT INTO `sys_log` VALUES (3603, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":3,\"menus\":[{\"subCount\":0,\"id\":118,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":124,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":126,\"menuSort\":999},{\"subCount\":0,\"id\":127,\"menuSort\":999},{\"subCount\":0,\"id\":128,\"menuSort\":999},{\"subCount\":0,\"id\":1,\"menuSort\":999},{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":132,\"menuSort\":999},{\"subCount\":0,\"id\":133,\"menuSort\":999},{\"subCount\":0,\"id\":6,\"menuSort\":999},{\"subCount\":0,\"id\":134,\"menuSort\":999},{\"subCount\":0,\"id\":7,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":136,\"menuSort\":999},{\"subCount\":0,\"id\":9,\"menuSort\":999},{\"subCount\":0,\"id\":137,\"menuSort\":999},{\"subCount\":0,\"id\":138,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":140,\"menuSort\":999},{\"subCount\":0,\"id\":141,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999},{\"subCount\":0,\"id\":143,\"menuSort\":999},{\"subCount\":0,\"id\":144,\"menuSort\":999},{\"subCount\":0,\"id\":145,\"menuSort\":999},{\"subCount\":0,\"id\":146,\"menuSort\":999},{\"subCount\":0,\"id\":147,\"menuSort\":999},{\"subCount\":0,\"id\":148,\"menuSort\":999},{\"subCount\":0,\"id\":32,\"menuSort\":999},{\"subCount\":0,\"id\":37,\"menuSort\":999},{\"subCount\":0,\"id\":39,\"menuSort\":999},{\"subCount\":0,\"id\":41,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":46,\"menuSort\":999},{\"subCount\":0,\"id\":48,\"menuSort\":999},{\"subCount\":0,\"id\":49,\"menuSort\":999},{\"subCount\":0,\"id\":50,\"menuSort\":999},{\"subCount\":0,\"id\":60,\"menuSort\":999},{\"subCount\":0,\"id\":61,\"menuSort\":999},{\"subCount\":0,\"id\":62,\"menuSort\":999},{\"subCount\":0,\"id\":64,\"menuSort\":999},{\"subCount\":0,\"id\":65,\"menuSort\":999},{\"subCount\":0,\"id\":66,\"menuSort\":999},{\"subCount\":0,\"id\":80,\"menuSort\":999}]}', '192.168.244.1', 36, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:19:11');
INSERT INTO `sys_log` VALUES (3604, '查询member', 'INFO', 'com.bigtime.rest.MemberController.query()', '', '192.168.244.1', 6, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:19:30');
INSERT INTO `sys_log` VALUES (3605, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":true,\"roles\":[],\"icon\":\"user\",\"permission\":\"staff:list\",\"pid\":118,\"updateTime\":1615275620000,\"title\":\"员工管理\",\"type\":1,\"subCount\":3,\"path\":\"staff\",\"component\":\"bigtime/staff/index\",\"createBy\":\"admin\",\"createTime\":1615275505000,\"iFrame\":false,\"id\":149,\"componentName\":\"Staff\",\"menuSort\":1}', '192.168.244.1', 62, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:23:05');
INSERT INTO `sys_log` VALUES (3606, '修改菜单', 'INFO', 'me.zhengjie.modules.system.rest.MenuController.update()', '{\"cache\":false,\"hidden\":true,\"roles\":[],\"icon\":\"user\",\"permission\":\"member:list\",\"pid\":118,\"updateTime\":1615275620000,\"title\":\"会员管理\",\"type\":1,\"subCount\":3,\"path\":\"member\",\"component\":\"bigtime/member/index\",\"createBy\":\"admin\",\"createTime\":1615275505000,\"iFrame\":false,\"id\":119,\"componentName\":\"Member\",\"menuSort\":1}', '192.168.244.1', 53, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:23:13');
INSERT INTO `sys_log` VALUES (3607, '查询combo', 'INFO', 'com.bigtime.rest.ComboController.query()', '', '192.168.244.1', 6, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:23:28');
INSERT INTO `sys_log` VALUES (3608, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":4,\"menus\":[{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":118,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":124,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":126,\"menuSort\":999},{\"subCount\":0,\"id\":127,\"menuSort\":999},{\"subCount\":0,\"id\":128,\"menuSort\":999},{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":132,\"menuSort\":999},{\"subCount\":0,\"id\":133,\"menuSort\":999},{\"subCount\":0,\"id\":134,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":136,\"menuSort\":999},{\"subCount\":0,\"id\":137,\"menuSort\":999},{\"subCount\":0,\"id\":138,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":140,\"menuSort\":999},{\"subCount\":0,\"id\":141,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999},{\"subCount\":0,\"id\":143,\"menuSort\":999},{\"subCount\":0,\"id\":144,\"menuSort\":999},{\"subCount\":0,\"id\":145,\"menuSort\":999},{\"subCount\":0,\"id\":146,\"menuSort\":999},{\"subCount\":0,\"id\":147,\"menuSort\":999},{\"subCount\":0,\"id\":148,\"menuSort\":999},{\"subCount\":0,\"id\":149,\"menuSort\":999},{\"subCount\":0,\"id\":150,\"menuSort\":999},{\"subCount\":0,\"id\":151,\"menuSort\":999},{\"subCount\":0,\"id\":152,\"menuSort\":999}]}', '192.168.244.1', 76, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:21');
INSERT INTO `sys_log` VALUES (3609, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":3,\"menus\":[{\"subCount\":0,\"id\":118,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":124,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999},{\"subCount\":0,\"id\":126,\"menuSort\":999},{\"subCount\":0,\"id\":127,\"menuSort\":999},{\"subCount\":0,\"id\":128,\"menuSort\":999},{\"subCount\":0,\"id\":1,\"menuSort\":999},{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":3,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":132,\"menuSort\":999},{\"subCount\":0,\"id\":133,\"menuSort\":999},{\"subCount\":0,\"id\":6,\"menuSort\":999},{\"subCount\":0,\"id\":134,\"menuSort\":999},{\"subCount\":0,\"id\":7,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":136,\"menuSort\":999},{\"subCount\":0,\"id\":9,\"menuSort\":999},{\"subCount\":0,\"id\":137,\"menuSort\":999},{\"subCount\":0,\"id\":138,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":140,\"menuSort\":999},{\"subCount\":0,\"id\":141,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999},{\"subCount\":0,\"id\":143,\"menuSort\":999},{\"subCount\":0,\"id\":144,\"menuSort\":999},{\"subCount\":0,\"id\":145,\"menuSort\":999},{\"subCount\":0,\"id\":146,\"menuSort\":999},{\"subCount\":0,\"id\":147,\"menuSort\":999},{\"subCount\":0,\"id\":148,\"menuSort\":999},{\"subCount\":0,\"id\":149,\"menuSort\":999},{\"subCount\":0,\"id\":150,\"menuSort\":999},{\"subCount\":0,\"id\":151,\"menuSort\":999},{\"subCount\":0,\"id\":152,\"menuSort\":999},{\"subCount\":0,\"id\":32,\"menuSort\":999},{\"subCount\":0,\"id\":37,\"menuSort\":999},{\"subCount\":0,\"id\":39,\"menuSort\":999},{\"subCount\":0,\"id\":41,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":45,\"menuSort\":999},{\"subCount\":0,\"id\":46,\"menuSort\":999},{\"subCount\":0,\"id\":48,\"menuSort\":999},{\"subCount\":0,\"id\":49,\"menuSort\":999},{\"subCount\":0,\"id\":50,\"menuSort\":999},{\"subCount\":0,\"id\":60,\"menuSort\":999},{\"subCount\":0,\"id\":61,\"menuSort\":999},{\"subCount\":0,\"id\":62,\"menuSort\":999},{\"subCount\":0,\"id\":64,\"menuSort\":999},{\"subCount\":0,\"id\":65,\"menuSort\":999},{\"subCount\":0,\"id\":66,\"menuSort\":999},{\"subCount\":0,\"id\":80,\"menuSort\":999}]}', '192.168.244.1', 55, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:27');
INSERT INTO `sys_log` VALUES (3610, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":2,\"menus\":[{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999},{\"subCount\":0,\"id\":145,\"menuSort\":999},{\"subCount\":0,\"id\":146,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999}]}', '192.168.244.1', 33, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:38');
INSERT INTO `sys_log` VALUES (3611, '修改角色菜单', 'INFO', 'me.zhengjie.modules.system.rest.RoleController.updateMenu()', '{\"level\":3,\"dataScope\":\"本级\",\"id\":2,\"menus\":[{\"subCount\":0,\"id\":129,\"menuSort\":999},{\"subCount\":0,\"id\":130,\"menuSort\":999},{\"subCount\":0,\"id\":2,\"menuSort\":999},{\"subCount\":0,\"id\":131,\"menuSort\":999},{\"subCount\":0,\"id\":135,\"menuSort\":999},{\"subCount\":0,\"id\":139,\"menuSort\":999},{\"subCount\":0,\"id\":44,\"menuSort\":999},{\"subCount\":0,\"id\":142,\"menuSort\":999},{\"subCount\":0,\"id\":145,\"menuSort\":999},{\"subCount\":0,\"id\":146,\"menuSort\":999},{\"subCount\":0,\"id\":119,\"menuSort\":999},{\"subCount\":0,\"id\":122,\"menuSort\":999},{\"subCount\":0,\"id\":123,\"menuSort\":999},{\"subCount\":0,\"id\":125,\"menuSort\":999}]}', '192.168.244.1', 36, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:47');
INSERT INTO `sys_log` VALUES (3612, '查询combo', 'INFO', 'com.bigtime.rest.ComboController.query()', '', '192.168.244.1', 7, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:52');
INSERT INTO `sys_log` VALUES (3613, '查询rechargerecord', 'INFO', 'com.bigtime.rest.RechargeRecordController.query()', '', '192.168.244.1', 10, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:55');
INSERT INTO `sys_log` VALUES (3614, '查询playercashlog', 'INFO', 'com.bigtime.rest.PlayerCashlogController.query()', '', '192.168.244.1', 5, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:56');
INSERT INTO `sys_log` VALUES (3615, '查询toy', 'INFO', 'com.bigtime.rest.ToyController.query()', '', '192.168.244.1', 10, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:56');
INSERT INTO `sys_log` VALUES (3616, '查询playercashlog', 'INFO', 'com.bigtime.rest.PlayerCashlogController.query()', '', '192.168.244.1', 5, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:58');
INSERT INTO `sys_log` VALUES (3617, '查询combo', 'INFO', 'com.bigtime.rest.ComboController.query()', '', '192.168.244.1', 7, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:24:59');
INSERT INTO `sys_log` VALUES (3618, '新增用户', 'INFO', 'me.zhengjie.modules.system.rest.UserController.create()', '{\"gender\":\"男\",\"nickName\":\"大老板\",\"roles\":[{\"level\":3,\"dataScope\":\"本级\",\"id\":3}],\"jobs\":[{\"id\":14}],\"updateTime\":1619746004377,\"dept\":{\"subCount\":0,\"id\":5},\"isAdmin\":false,\"enabled\":true,\"password\":\"$2a$10$nns8tSfMfXvCcKWeWS1ukOj99f1yhH3t.kv58/gtvBu3oWRxFQsfy\",\"createBy\":\"admin\",\"phone\":\"18188888888\",\"updateBy\":\"admin\",\"createTime\":1619746004377,\"id\":3,\"email\":\"18188888888@qq.com\",\"username\":\"999\"}', '192.168.244.1', 119, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:26:44');
INSERT INTO `sys_log` VALUES (3619, '查询rechargerecord', 'INFO', 'com.bigtime.rest.RechargeRecordController.query()', '', '192.168.244.1', 6, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:31:02');
INSERT INTO `sys_log` VALUES (3620, '查询combo', 'INFO', 'com.bigtime.rest.ComboController.query()', '', '192.168.244.1', 5, 'admin', '内网IP', 'Chrome 8', NULL, '2021-04-30 09:31:09');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` bigint NULL DEFAULT NULL COMMENT '上级菜单ID',
  `sub_count` int NULL DEFAULT 0 COMMENT '子菜单数目',
  `type` int NULL DEFAULT NULL COMMENT '菜单类型',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '菜单标题',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件名称',
  `component` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '组件',
  `menu_sort` int NULL DEFAULT NULL COMMENT '排序',
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '链接地址',
  `i_frame` bit(1) NULL DEFAULT NULL COMMENT '是否外链',
  `cache` bit(1) NULL DEFAULT b'0' COMMENT '缓存',
  `hidden` bit(1) NULL DEFAULT b'0' COMMENT '隐藏',
  `permission` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '权限',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`menu_id`) USING BTREE,
  UNIQUE INDEX `uniq_title`(`title`) USING BTREE,
  UNIQUE INDEX `uniq_name`(`name`) USING BTREE,
  INDEX `inx_pid`(`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 147 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统菜单' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, NULL, 7, 0, '系统管理', NULL, NULL, 1, 'system', 'system', b'0', b'0', b'0', NULL, NULL, NULL, '2018-12-18 15:11:29', NULL);
INSERT INTO `sys_menu` VALUES (2, 1, 3, 1, '用户管理', 'User', 'system/user/index', 2, 'peoples', 'user', b'0', b'0', b'0', 'user:list', NULL, NULL, '2018-12-18 15:14:44', NULL);
INSERT INTO `sys_menu` VALUES (3, 1, 3, 1, '角色管理', 'Role', 'system/role/index', 3, 'role', 'role', b'0', b'0', b'0', 'roles:list', NULL, NULL, '2018-12-18 15:16:07', NULL);
INSERT INTO `sys_menu` VALUES (5, 1, 3, 1, '菜单管理', 'Menu', 'system/menu/index', 5, 'menu', 'menu', b'0', b'0', b'0', 'menu:list', NULL, NULL, '2018-12-18 15:17:28', NULL);
INSERT INTO `sys_menu` VALUES (6, NULL, 5, 0, '系统监控', NULL, NULL, 10, 'monitor', 'monitor', b'0', b'0', b'0', NULL, NULL, NULL, '2018-12-18 15:17:48', NULL);
INSERT INTO `sys_menu` VALUES (7, 6, 0, 1, '操作日志', 'Log', 'monitor/log/index', 11, 'log', 'logs', b'0', b'1', b'0', NULL, NULL, 'admin', '2018-12-18 15:18:26', '2020-06-06 13:11:57');
INSERT INTO `sys_menu` VALUES (9, 6, 0, 1, 'SQL监控', 'Sql', 'monitor/sql/index', 18, 'sqlMonitor', 'druid', b'0', b'0', b'0', NULL, NULL, NULL, '2018-12-18 15:19:34', NULL);
INSERT INTO `sys_menu` VALUES (10, NULL, 5, 0, '组件管理', NULL, NULL, 50, 'zujian', 'components', b'0', b'0', b'0', NULL, NULL, NULL, '2018-12-19 13:38:16', NULL);
INSERT INTO `sys_menu` VALUES (11, 10, 0, 1, '图标库', 'Icons', 'components/icons/index', 51, 'icon', 'icon', b'0', b'0', b'0', NULL, NULL, NULL, '2018-12-19 13:38:49', NULL);
INSERT INTO `sys_menu` VALUES (14, 36, 0, 1, '邮件工具', 'Email', 'tools/email/index', 35, 'email', 'email', b'0', b'0', b'0', NULL, NULL, NULL, '2018-12-27 10:13:09', NULL);
INSERT INTO `sys_menu` VALUES (15, 10, 0, 1, '富文本', 'Editor', 'components/Editor', 52, 'fwb', 'tinymce', b'0', b'0', b'0', NULL, NULL, NULL, '2018-12-27 11:58:25', NULL);
INSERT INTO `sys_menu` VALUES (18, 36, 3, 1, '存储管理', 'Storage', 'tools/storage/index', 34, 'qiniu', 'storage', b'0', b'0', b'0', 'storage:list', NULL, NULL, '2018-12-31 11:12:15', NULL);
INSERT INTO `sys_menu` VALUES (19, 36, 0, 1, '支付宝工具', 'AliPay', 'tools/aliPay/index', 37, 'alipay', 'aliPay', b'0', b'0', b'0', NULL, NULL, NULL, '2018-12-31 14:52:38', NULL);
INSERT INTO `sys_menu` VALUES (21, NULL, 2, 0, '多级菜单', NULL, '', 900, 'menu', 'nested', b'0', b'0', b'0', NULL, NULL, 'admin', '2019-01-04 16:22:03', '2020-06-21 17:27:35');
INSERT INTO `sys_menu` VALUES (22, 21, 2, 0, '二级菜单1', NULL, '', 999, 'menu', 'menu1', b'0', b'0', b'0', NULL, NULL, 'admin', '2019-01-04 16:23:29', '2020-06-21 17:27:20');
INSERT INTO `sys_menu` VALUES (23, 21, 0, 1, '二级菜单2', NULL, 'nested/menu2/index', 999, 'menu', 'menu2', b'0', b'0', b'0', NULL, NULL, NULL, '2019-01-04 16:23:57', NULL);
INSERT INTO `sys_menu` VALUES (24, 22, 0, 1, '三级菜单1', 'Test', 'nested/menu1/menu1-1', 999, 'menu', 'menu1-1', b'0', b'0', b'0', NULL, NULL, NULL, '2019-01-04 16:24:48', NULL);
INSERT INTO `sys_menu` VALUES (27, 22, 0, 1, '三级菜单2', NULL, 'nested/menu1/menu1-2', 999, 'menu', 'menu1-2', b'0', b'0', b'0', NULL, NULL, NULL, '2019-01-07 17:27:32', NULL);
INSERT INTO `sys_menu` VALUES (28, 1, 3, 1, '任务调度', 'Timing', 'system/timing/index', 999, 'timing', 'timing', b'0', b'0', b'0', 'timing:list', NULL, NULL, '2019-01-07 20:34:40', NULL);
INSERT INTO `sys_menu` VALUES (30, 36, 0, 1, '代码生成', 'GeneratorIndex', 'generator/index', 32, 'dev', 'generator', b'0', b'1', b'0', NULL, NULL, NULL, '2019-01-11 15:45:55', NULL);
INSERT INTO `sys_menu` VALUES (32, 6, 0, 1, '异常日志', 'ErrorLog', 'monitor/log/errorLog', 12, 'error', 'errorLog', b'0', b'0', b'0', NULL, NULL, NULL, '2019-01-13 13:49:03', NULL);
INSERT INTO `sys_menu` VALUES (33, 10, 0, 1, 'Markdown', 'Markdown', 'components/MarkDown', 53, 'markdown', 'markdown', b'0', b'0', b'0', NULL, NULL, NULL, '2019-03-08 13:46:44', NULL);
INSERT INTO `sys_menu` VALUES (34, 10, 0, 1, 'Yaml编辑器', 'YamlEdit', 'components/YamlEdit', 54, 'dev', 'yaml', b'0', b'0', b'0', NULL, NULL, NULL, '2019-03-08 15:49:40', NULL);
INSERT INTO `sys_menu` VALUES (35, 1, 3, 1, '部门管理', 'Dept', 'system/dept/index', 6, 'dept', 'dept', b'0', b'0', b'0', 'dept:list', NULL, NULL, '2019-03-25 09:46:00', NULL);
INSERT INTO `sys_menu` VALUES (36, NULL, 7, 0, '系统工具', NULL, '', 30, 'sys-tools', 'sys-tools', b'0', b'0', b'0', NULL, NULL, NULL, '2019-03-29 10:57:35', NULL);
INSERT INTO `sys_menu` VALUES (37, 1, 3, 1, '岗位管理', 'Job', 'system/job/index', 7, 'Steve-Jobs', 'job', b'0', b'0', b'0', 'job:list', NULL, NULL, '2019-03-29 13:51:18', NULL);
INSERT INTO `sys_menu` VALUES (38, 36, 0, 1, '接口文档', 'Swagger', 'tools/swagger/index', 36, 'swagger', 'swagger2', b'0', b'0', b'0', NULL, NULL, NULL, '2019-03-29 19:57:53', NULL);
INSERT INTO `sys_menu` VALUES (39, 1, 3, 1, '字典管理', 'Dict', 'system/dict/index', 8, 'dictionary', 'dict', b'0', b'0', b'0', 'dict:list', NULL, NULL, '2019-04-10 11:49:04', NULL);
INSERT INTO `sys_menu` VALUES (41, 6, 0, 1, '在线用户', 'OnlineUser', 'monitor/online/index', 10, 'Steve-Jobs', 'online', b'0', b'0', b'0', NULL, NULL, NULL, '2019-10-26 22:08:43', NULL);
INSERT INTO `sys_menu` VALUES (44, 2, 0, 2, '用户新增', NULL, '', 2, '', '', b'0', b'0', b'0', 'user:add', NULL, NULL, '2019-10-29 10:59:46', NULL);
INSERT INTO `sys_menu` VALUES (45, 2, 0, 2, '用户编辑', NULL, '', 3, '', '', b'0', b'0', b'0', 'user:edit', NULL, NULL, '2019-10-29 11:00:08', NULL);
INSERT INTO `sys_menu` VALUES (46, 2, 0, 2, '用户删除', NULL, '', 4, '', '', b'0', b'0', b'0', 'user:del', NULL, NULL, '2019-10-29 11:00:23', NULL);
INSERT INTO `sys_menu` VALUES (48, 3, 0, 2, '角色创建', NULL, '', 2, '', '', b'0', b'0', b'0', 'roles:add', NULL, NULL, '2019-10-29 12:45:34', NULL);
INSERT INTO `sys_menu` VALUES (49, 3, 0, 2, '角色修改', NULL, '', 3, '', '', b'0', b'0', b'0', 'roles:edit', NULL, NULL, '2019-10-29 12:46:16', NULL);
INSERT INTO `sys_menu` VALUES (50, 3, 0, 2, '角色删除', NULL, '', 4, '', '', b'0', b'0', b'0', 'roles:del', NULL, NULL, '2019-10-29 12:46:51', NULL);
INSERT INTO `sys_menu` VALUES (52, 5, 0, 2, '菜单新增', NULL, '', 2, '', '', b'0', b'0', b'0', 'menu:add', NULL, NULL, '2019-10-29 12:55:07', NULL);
INSERT INTO `sys_menu` VALUES (53, 5, 0, 2, '菜单编辑', NULL, '', 3, '', '', b'0', b'0', b'0', 'menu:edit', NULL, NULL, '2019-10-29 12:55:40', NULL);
INSERT INTO `sys_menu` VALUES (54, 5, 0, 2, '菜单删除', NULL, '', 4, '', '', b'0', b'0', b'0', 'menu:del', NULL, NULL, '2019-10-29 12:56:00', NULL);
INSERT INTO `sys_menu` VALUES (56, 35, 0, 2, '部门新增', NULL, '', 2, '', '', b'0', b'0', b'0', 'dept:add', NULL, NULL, '2019-10-29 12:57:09', NULL);
INSERT INTO `sys_menu` VALUES (57, 35, 0, 2, '部门编辑', NULL, '', 3, '', '', b'0', b'0', b'0', 'dept:edit', NULL, NULL, '2019-10-29 12:57:27', NULL);
INSERT INTO `sys_menu` VALUES (58, 35, 0, 2, '部门删除', NULL, '', 4, '', '', b'0', b'0', b'0', 'dept:del', NULL, NULL, '2019-10-29 12:57:41', NULL);
INSERT INTO `sys_menu` VALUES (60, 37, 0, 2, '岗位新增', NULL, '', 2, '', '', b'0', b'0', b'0', 'job:add', NULL, NULL, '2019-10-29 12:58:27', NULL);
INSERT INTO `sys_menu` VALUES (61, 37, 0, 2, '岗位编辑', NULL, '', 3, '', '', b'0', b'0', b'0', 'job:edit', NULL, NULL, '2019-10-29 12:58:45', NULL);
INSERT INTO `sys_menu` VALUES (62, 37, 0, 2, '岗位删除', NULL, '', 4, '', '', b'0', b'0', b'0', 'job:del', NULL, NULL, '2019-10-29 12:59:04', NULL);
INSERT INTO `sys_menu` VALUES (64, 39, 0, 2, '字典新增', NULL, '', 2, '', '', b'0', b'0', b'0', 'dict:add', NULL, NULL, '2019-10-29 13:00:17', NULL);
INSERT INTO `sys_menu` VALUES (65, 39, 0, 2, '字典编辑', NULL, '', 3, '', '', b'0', b'0', b'0', 'dict:edit', NULL, NULL, '2019-10-29 13:00:42', NULL);
INSERT INTO `sys_menu` VALUES (66, 39, 0, 2, '字典删除', NULL, '', 4, '', '', b'0', b'0', b'0', 'dict:del', NULL, NULL, '2019-10-29 13:00:59', NULL);
INSERT INTO `sys_menu` VALUES (73, 28, 0, 2, '任务新增', NULL, '', 2, '', '', b'0', b'0', b'0', 'timing:add', NULL, NULL, '2019-10-29 13:07:28', NULL);
INSERT INTO `sys_menu` VALUES (74, 28, 0, 2, '任务编辑', NULL, '', 3, '', '', b'0', b'0', b'0', 'timing:edit', NULL, NULL, '2019-10-29 13:07:41', NULL);
INSERT INTO `sys_menu` VALUES (75, 28, 0, 2, '任务删除', NULL, '', 4, '', '', b'0', b'0', b'0', 'timing:del', NULL, NULL, '2019-10-29 13:07:54', NULL);
INSERT INTO `sys_menu` VALUES (77, 18, 0, 2, '上传文件', NULL, '', 2, '', '', b'0', b'0', b'0', 'storage:add', NULL, NULL, '2019-10-29 13:09:09', NULL);
INSERT INTO `sys_menu` VALUES (78, 18, 0, 2, '文件编辑', NULL, '', 3, '', '', b'0', b'0', b'0', 'storage:edit', NULL, NULL, '2019-10-29 13:09:22', NULL);
INSERT INTO `sys_menu` VALUES (79, 18, 0, 2, '文件删除', NULL, '', 4, '', '', b'0', b'0', b'0', 'storage:del', NULL, NULL, '2019-10-29 13:09:34', NULL);
INSERT INTO `sys_menu` VALUES (80, 6, 0, 1, '服务监控', 'ServerMonitor', 'monitor/server/index', 14, 'codeConsole', 'server', b'0', b'0', b'0', 'monitor:list', NULL, 'admin', '2019-11-07 13:06:39', '2020-05-04 18:20:50');
INSERT INTO `sys_menu` VALUES (82, 36, 0, 1, '生成配置', 'GeneratorConfig', 'generator/config', 33, 'dev', 'generator/config/:tableName', b'0', b'1', b'1', '', NULL, NULL, '2019-11-17 20:08:56', NULL);
INSERT INTO `sys_menu` VALUES (83, 10, 0, 1, '图表库', 'Echarts', 'components/Echarts', 50, 'chart', 'echarts', b'0', b'1', b'0', '', NULL, NULL, '2019-11-21 09:04:32', NULL);
INSERT INTO `sys_menu` VALUES (90, NULL, 5, 1, '运维管理', 'Mnt', '', 20, 'mnt', 'mnt', b'0', b'0', b'0', NULL, NULL, NULL, '2019-11-09 10:31:08', NULL);
INSERT INTO `sys_menu` VALUES (92, 90, 3, 1, '服务器', 'ServerDeploy', 'mnt/server/index', 22, 'server', 'mnt/serverDeploy', b'0', b'0', b'0', 'serverDeploy:list', NULL, NULL, '2019-11-10 10:29:25', NULL);
INSERT INTO `sys_menu` VALUES (93, 90, 3, 1, '应用管理', 'App', 'mnt/app/index', 23, 'app', 'mnt/app', b'0', b'0', b'0', 'app:list', NULL, NULL, '2019-11-10 11:05:16', NULL);
INSERT INTO `sys_menu` VALUES (94, 90, 3, 1, '部署管理', 'Deploy', 'mnt/deploy/index', 24, 'deploy', 'mnt/deploy', b'0', b'0', b'0', 'deploy:list', NULL, NULL, '2019-11-10 15:56:55', NULL);
INSERT INTO `sys_menu` VALUES (97, 90, 1, 1, '部署备份', 'DeployHistory', 'mnt/deployHistory/index', 25, 'backup', 'mnt/deployHistory', b'0', b'0', b'0', 'deployHistory:list', NULL, NULL, '2019-11-10 16:49:44', NULL);
INSERT INTO `sys_menu` VALUES (98, 90, 3, 1, '数据库管理', 'Database', 'mnt/database/index', 26, 'database', 'mnt/database', b'0', b'0', b'0', 'database:list', NULL, NULL, '2019-11-10 20:40:04', NULL);
INSERT INTO `sys_menu` VALUES (102, 97, 0, 2, '删除', NULL, '', 999, '', '', b'0', b'0', b'0', 'deployHistory:del', NULL, NULL, '2019-11-17 09:32:48', NULL);
INSERT INTO `sys_menu` VALUES (103, 92, 0, 2, '服务器新增', NULL, '', 999, '', '', b'0', b'0', b'0', 'serverDeploy:add', NULL, NULL, '2019-11-17 11:08:33', NULL);
INSERT INTO `sys_menu` VALUES (104, 92, 0, 2, '服务器编辑', NULL, '', 999, '', '', b'0', b'0', b'0', 'serverDeploy:edit', NULL, NULL, '2019-11-17 11:08:57', NULL);
INSERT INTO `sys_menu` VALUES (105, 92, 0, 2, '服务器删除', NULL, '', 999, '', '', b'0', b'0', b'0', 'serverDeploy:del', NULL, NULL, '2019-11-17 11:09:15', NULL);
INSERT INTO `sys_menu` VALUES (106, 93, 0, 2, '应用新增', NULL, '', 999, '', '', b'0', b'0', b'0', 'app:add', NULL, NULL, '2019-11-17 11:10:03', NULL);
INSERT INTO `sys_menu` VALUES (107, 93, 0, 2, '应用编辑', NULL, '', 999, '', '', b'0', b'0', b'0', 'app:edit', NULL, NULL, '2019-11-17 11:10:28', NULL);
INSERT INTO `sys_menu` VALUES (108, 93, 0, 2, '应用删除', NULL, '', 999, '', '', b'0', b'0', b'0', 'app:del', NULL, NULL, '2019-11-17 11:10:55', NULL);
INSERT INTO `sys_menu` VALUES (109, 94, 0, 2, '部署新增', NULL, '', 999, '', '', b'0', b'0', b'0', 'deploy:add', NULL, NULL, '2019-11-17 11:11:22', NULL);
INSERT INTO `sys_menu` VALUES (110, 94, 0, 2, '部署编辑', NULL, '', 999, '', '', b'0', b'0', b'0', 'deploy:edit', NULL, NULL, '2019-11-17 11:11:41', NULL);
INSERT INTO `sys_menu` VALUES (111, 94, 0, 2, '部署删除', NULL, '', 999, '', '', b'0', b'0', b'0', 'deploy:del', NULL, NULL, '2019-11-17 11:12:01', NULL);
INSERT INTO `sys_menu` VALUES (112, 98, 0, 2, '数据库新增', NULL, '', 999, '', '', b'0', b'0', b'0', 'database:add', NULL, NULL, '2019-11-17 11:12:43', NULL);
INSERT INTO `sys_menu` VALUES (113, 98, 0, 2, '数据库编辑', NULL, '', 999, '', '', b'0', b'0', b'0', 'database:edit', NULL, NULL, '2019-11-17 11:12:58', NULL);
INSERT INTO `sys_menu` VALUES (114, 98, 0, 2, '数据库删除', NULL, '', 999, '', '', b'0', b'0', b'0', 'database:del', NULL, NULL, '2019-11-17 11:13:14', NULL);
INSERT INTO `sys_menu` VALUES (116, 36, 0, 1, '生成预览', 'Preview', 'generator/preview', 999, 'java', 'generator/preview/:tableName', b'0', b'1', b'1', NULL, NULL, NULL, '2019-11-26 14:54:36', NULL);
INSERT INTO `sys_menu` VALUES (118, NULL, 8, 0, '数据管理', NULL, NULL, 0, 'icon', 'shop', b'0', b'0', b'0', NULL, 'admin', 'admin', '2021-03-09 15:32:06', '2021-03-09 15:42:50');
INSERT INTO `sys_menu` VALUES (119, 118, 3, 1, '会员管理', 'Member', 'bigtime/member/index', 1, 'user', 'member', b'0', b'0', b'1', 'member:list', 'admin', 'admin', '2021-03-09 15:38:25', '2021-04-30 09:23:13');
INSERT INTO `sys_menu` VALUES (122, 119, 0, 2, '会员添加', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', 'member:add', 'admin', 'admin', '2021-03-09 15:48:29', '2021-03-09 15:48:49');
INSERT INTO `sys_menu` VALUES (123, 119, 0, 2, '会员修改', NULL, NULL, 2, NULL, NULL, b'0', b'0', b'0', 'member:update', 'admin', 'admin', '2021-03-09 15:49:18', '2021-03-09 15:49:18');
INSERT INTO `sys_menu` VALUES (124, 119, 0, 2, '会员删除', NULL, NULL, 3, NULL, NULL, b'0', b'0', b'0', 'member:del', 'admin', 'admin', '2021-03-09 15:49:47', '2021-03-09 15:49:47');
INSERT INTO `sys_menu` VALUES (125, 118, 3, 1, '套餐管理', 'Combo', 'bigtime/combo/index', 1, 'user', 'combo', b'0', b'0', b'0', 'combo:list', 'admin', 'admin', '2021-03-09 15:38:25', '2021-03-09 15:40:20');
INSERT INTO `sys_menu` VALUES (126, 125, 0, 2, '套餐添加', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', 'combo:add', 'admin', 'admin', '2021-03-09 15:48:29', '2021-03-09 15:48:49');
INSERT INTO `sys_menu` VALUES (127, 125, 0, 2, '套餐修改', NULL, NULL, 2, NULL, NULL, b'0', b'0', b'0', 'combo:update', 'admin', 'admin', '2021-03-09 15:49:18', '2021-03-09 15:49:18');
INSERT INTO `sys_menu` VALUES (128, 125, 0, 2, '套餐删除', NULL, NULL, 3, NULL, NULL, b'0', b'0', b'0', 'combo:del', 'admin', 'admin', '2021-03-09 15:49:47', '2021-03-09 15:49:47');
INSERT INTO `sys_menu` VALUES (129, 118, 3, 1, '充值记录', 'RechargeRecord', 'bigtime/rechargerecord/index', 1, 'money', 'rechargerecord', b'0', b'0', b'0', 'rechargerecord:list', 'admin', 'admin', '2021-03-09 15:58:52', '2021-03-09 16:50:23');
INSERT INTO `sys_menu` VALUES (130, 118, 3, 1, '消费记录', 'PlayerCashlog', 'bigtime/playercashlog/index', 1, 'money', 'playercashlog', b'0', b'0', b'0', 'playercashlog:list', 'admin', 'admin', '2021-03-09 15:58:52', '2021-03-09 16:50:23');
INSERT INTO `sys_menu` VALUES (131, 118, 3, 1, '店铺管理', 'Shop', 'bigtime/shop/index', 1, 'user', 'shop', b'0', b'0', b'0', 'shop:list', 'admin', 'admin', '2021-03-09 15:38:25', '2021-03-09 15:40:20');
INSERT INTO `sys_menu` VALUES (132, 131, 0, 2, '店铺添加', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', 'shop:add', 'admin', 'admin', '2021-03-09 15:48:29', '2021-03-09 15:48:49');
INSERT INTO `sys_menu` VALUES (133, 131, 0, 2, '店铺修改', NULL, NULL, 2, NULL, NULL, b'0', b'0', b'0', 'shop:update', 'admin', 'admin', '2021-03-09 15:49:18', '2021-03-09 15:49:18');
INSERT INTO `sys_menu` VALUES (134, 131, 0, 2, '店铺删除', NULL, NULL, 3, NULL, NULL, b'0', b'0', b'0', 'shop:del', 'admin', 'admin', '2021-03-09 15:49:47', '2021-03-09 15:49:47');
INSERT INTO `sys_menu` VALUES (135, 118, 3, 1, '玩具管理', 'Toy', 'bigtime/toy/index', 1, 'user', 'toy', b'0', b'0', b'0', 'toy:list', 'admin', 'admin', '2021-03-09 15:38:25', '2021-03-09 15:40:20');
INSERT INTO `sys_menu` VALUES (136, 135, 0, 2, '玩具添加', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', 'toy:add', 'admin', 'admin', '2021-03-09 15:48:29', '2021-03-09 15:48:49');
INSERT INTO `sys_menu` VALUES (137, 135, 0, 2, '玩具修改', NULL, NULL, 2, NULL, NULL, b'0', b'0', b'0', 'toy:update', 'admin', 'admin', '2021-03-09 15:49:18', '2021-03-09 15:49:18');
INSERT INTO `sys_menu` VALUES (138, 135, 0, 2, '玩具删除', NULL, NULL, 3, NULL, NULL, b'0', b'0', b'0', 'toy:del', 'admin', 'admin', '2021-03-09 15:49:47', '2021-03-09 15:49:47');
INSERT INTO `sys_menu` VALUES (139, 129, 0, 2, '添加充值记录', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', 'rechargerecord:add', 'admin', 'admin', '2021-03-09 15:48:29', '2021-03-09 15:48:49');
INSERT INTO `sys_menu` VALUES (140, 129, 0, 2, '修改充值记录', NULL, NULL, 2, NULL, NULL, b'0', b'0', b'0', 'rechargerecord:update', 'admin', 'admin', '2021-03-09 15:49:18', '2021-03-09 15:49:18');
INSERT INTO `sys_menu` VALUES (141, 129, 0, 2, '删除充值记录', NULL, NULL, 3, NULL, NULL, b'0', b'0', b'0', 'rechargerecord:del', 'admin', 'admin', '2021-03-09 15:49:47', '2021-03-09 15:49:47');
INSERT INTO `sys_menu` VALUES (142, 130, 0, 2, '添加消费记录', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', 'playercashlog:add', 'admin', 'admin', '2021-03-09 15:48:29', '2021-03-09 15:48:49');
INSERT INTO `sys_menu` VALUES (143, 130, 0, 2, '修改消费记录', NULL, NULL, 2, NULL, NULL, b'0', b'0', b'0', 'playercashlog:update', 'admin', 'admin', '2021-03-09 15:49:18', '2021-03-09 15:49:18');
INSERT INTO `sys_menu` VALUES (144, 130, 0, 2, '删除消费记录', NULL, NULL, 3, NULL, NULL, b'0', b'0', b'0', 'playercashlog:del', 'admin', 'admin', '2021-03-09 15:49:47', '2021-03-09 15:49:47');
INSERT INTO `sys_menu` VALUES (145, 118, 3, 1, '消费记录明细', 'PlayerCashlogDetail', 'bigtime/playerCashlogDetail/index', 1, 'user', 'playerCashlogDetail', b'0', b'0', b'0', 'playerCashlogDetail:list', 'admin', 'admin', '2021-03-09 15:38:25', '2021-03-09 15:40:20');
INSERT INTO `sys_menu` VALUES (146, 145, 0, 2, '添加消费明细', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', 'playerCashlogDetail:add', 'admin', 'admin', '2021-03-09 15:48:29', '2021-03-09 15:48:49');
INSERT INTO `sys_menu` VALUES (147, 145, 0, 2, '修改消费明细', NULL, NULL, 2, NULL, NULL, b'0', b'0', b'0', 'playerCashlogDetail:update', 'admin', 'admin', '2021-03-09 15:49:18', '2021-03-09 15:49:18');
INSERT INTO `sys_menu` VALUES (148, 145, 0, 2, '删除消费明细', NULL, NULL, 3, NULL, NULL, b'0', b'0', b'0', 'playerCashlogDetail:del', 'admin', 'admin', '2021-03-09 15:49:47', '2021-03-09 15:49:47');
INSERT INTO `sys_menu` VALUES (149, 118, 3, 1, '员工管理', 'Staff', 'bigtime/staff/index', 1, 'user', 'staff', b'0', b'0', b'1', 'staff:list', 'admin', 'admin', '2021-03-09 15:38:25', '2021-04-30 09:23:05');
INSERT INTO `sys_menu` VALUES (150, 149, 0, 2, '添加员工', NULL, NULL, 1, NULL, NULL, b'0', b'0', b'0', 'staff:add', 'admin', 'admin', '2021-03-09 15:48:29', '2021-03-09 15:48:49');
INSERT INTO `sys_menu` VALUES (151, 149, 0, 2, '修改员工信息', NULL, NULL, 2, NULL, NULL, b'0', b'0', b'0', 'staff:update', 'admin', 'admin', '2021-03-09 15:49:18', '2021-03-09 15:49:18');
INSERT INTO `sys_menu` VALUES (152, 149, 0, 2, '删除员工', NULL, NULL, 3, NULL, NULL, b'0', b'0', b'0', 'staff:del', 'admin', 'admin', '2021-03-09 15:49:47', '2021-03-09 15:49:47');

-- ----------------------------
-- Table structure for sys_quartz_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz_job`;
CREATE TABLE `sys_quartz_job`  (
  `job_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `bean_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Spring Bean名称',
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'cron 表达式',
  `is_pause` bit(1) NULL DEFAULT NULL COMMENT '状态：1暂停、0启用',
  `job_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '任务名称',
  `method_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '方法名称',
  `params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参数',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `person_in_charge` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '负责人',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '报警邮箱',
  `sub_task` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '子任务ID',
  `pause_after_failure` bit(1) NULL DEFAULT NULL COMMENT '任务失败后是否暂停',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`job_id`) USING BTREE,
  INDEX `inx_is_pause`(`is_pause`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_quartz_job
-- ----------------------------
INSERT INTO `sys_quartz_job` VALUES (2, 'testTask', '0/5 * * * * ?', b'1', '测试1', 'run1', 'test', '带参测试，多参使用json', '测试', NULL, NULL, NULL, NULL, 'admin', '2019-08-22 14:08:29', '2020-05-24 13:58:33');
INSERT INTO `sys_quartz_job` VALUES (3, 'testTask', '0/5 * * * * ?', b'1', '测试', 'run', '', '不带参测试', 'Zheng Jie', '', '5,6', b'1', NULL, 'admin', '2019-09-26 16:44:39', '2020-05-24 14:48:12');
INSERT INTO `sys_quartz_job` VALUES (5, 'Test', '0/5 * * * * ?', b'1', '任务告警测试', 'run', NULL, '测试', 'test', '', NULL, b'1', 'admin', 'admin', '2020-05-05 20:32:41', '2020-05-05 20:36:13');
INSERT INTO `sys_quartz_job` VALUES (6, 'testTask', '0/5 * * * * ?', b'1', '测试3', 'run2', NULL, '测试3', 'Zheng Jie', '', NULL, b'1', 'admin', 'admin', '2020-05-05 20:35:41', '2020-05-05 20:36:07');

-- ----------------------------
-- Table structure for sys_quartz_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_quartz_log`;
CREATE TABLE `sys_quartz_log`  (
  `log_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `bean_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `cron_expression` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `exception_detail` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `is_success` bit(1) NULL DEFAULT NULL,
  `job_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `method_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `params` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `time` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`log_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '定时任务日志' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_quartz_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '名称',
  `level` int NULL DEFAULT NULL COMMENT '角色级别',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `data_scope` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据权限',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `uniq_name`(`name`) USING BTREE,
  INDEX `role_name_index`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 1, '-', '全部', NULL, 'admin', '2018-11-23 11:04:37', '2021-04-29 10:02:09');
INSERT INTO `sys_role` VALUES (2, '职员', 4, '-', '本级', NULL, 'admin', '2018-11-23 13:09:06', '2021-04-30 09:24:47');
INSERT INTO `sys_role` VALUES (3, '老板', 2, NULL, '本级', 'admin', 'admin', '2021-03-06 14:28:49', '2021-04-30 09:24:27');
INSERT INTO `sys_role` VALUES (4, '经理', 3, NULL, '本级', 'admin', 'admin', '2021-03-06 14:29:11', '2021-04-30 09:24:21');

-- ----------------------------
-- Table structure for sys_roles_depts
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles_depts`;
CREATE TABLE `sys_roles_depts`  (
  `role_id` bigint NOT NULL,
  `dept_id` bigint NOT NULL,
  PRIMARY KEY (`role_id`, `dept_id`) USING BTREE,
  INDEX `FK7qg6itn5ajdoa9h9o78v9ksur`(`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色部门关联' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_roles_depts
-- ----------------------------

-- ----------------------------
-- Table structure for sys_roles_menus
-- ----------------------------
DROP TABLE IF EXISTS `sys_roles_menus`;
CREATE TABLE `sys_roles_menus`  (
  `menu_id` bigint NOT NULL COMMENT '菜单ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`menu_id`, `role_id`) USING BTREE,
  INDEX `FKcngg2qadojhi3a651a5adkvbq`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色菜单关联' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_roles_menus
-- ----------------------------
INSERT INTO `sys_roles_menus` VALUES (1, 1);
INSERT INTO `sys_roles_menus` VALUES (2, 1);
INSERT INTO `sys_roles_menus` VALUES (3, 1);
INSERT INTO `sys_roles_menus` VALUES (5, 1);
INSERT INTO `sys_roles_menus` VALUES (6, 1);
INSERT INTO `sys_roles_menus` VALUES (7, 1);
INSERT INTO `sys_roles_menus` VALUES (9, 1);
INSERT INTO `sys_roles_menus` VALUES (10, 1);
INSERT INTO `sys_roles_menus` VALUES (11, 1);
INSERT INTO `sys_roles_menus` VALUES (14, 1);
INSERT INTO `sys_roles_menus` VALUES (15, 1);
INSERT INTO `sys_roles_menus` VALUES (18, 1);
INSERT INTO `sys_roles_menus` VALUES (19, 1);
INSERT INTO `sys_roles_menus` VALUES (21, 1);
INSERT INTO `sys_roles_menus` VALUES (22, 1);
INSERT INTO `sys_roles_menus` VALUES (23, 1);
INSERT INTO `sys_roles_menus` VALUES (24, 1);
INSERT INTO `sys_roles_menus` VALUES (27, 1);
INSERT INTO `sys_roles_menus` VALUES (28, 1);
INSERT INTO `sys_roles_menus` VALUES (30, 1);
INSERT INTO `sys_roles_menus` VALUES (32, 1);
INSERT INTO `sys_roles_menus` VALUES (33, 1);
INSERT INTO `sys_roles_menus` VALUES (34, 1);
INSERT INTO `sys_roles_menus` VALUES (35, 1);
INSERT INTO `sys_roles_menus` VALUES (36, 1);
INSERT INTO `sys_roles_menus` VALUES (37, 1);
INSERT INTO `sys_roles_menus` VALUES (38, 1);
INSERT INTO `sys_roles_menus` VALUES (39, 1);
INSERT INTO `sys_roles_menus` VALUES (41, 1);
INSERT INTO `sys_roles_menus` VALUES (44, 1);
INSERT INTO `sys_roles_menus` VALUES (45, 1);
INSERT INTO `sys_roles_menus` VALUES (46, 1);
INSERT INTO `sys_roles_menus` VALUES (48, 1);
INSERT INTO `sys_roles_menus` VALUES (49, 1);
INSERT INTO `sys_roles_menus` VALUES (50, 1);
INSERT INTO `sys_roles_menus` VALUES (52, 1);
INSERT INTO `sys_roles_menus` VALUES (53, 1);
INSERT INTO `sys_roles_menus` VALUES (54, 1);
INSERT INTO `sys_roles_menus` VALUES (56, 1);
INSERT INTO `sys_roles_menus` VALUES (57, 1);
INSERT INTO `sys_roles_menus` VALUES (58, 1);
INSERT INTO `sys_roles_menus` VALUES (60, 1);
INSERT INTO `sys_roles_menus` VALUES (61, 1);
INSERT INTO `sys_roles_menus` VALUES (62, 1);
INSERT INTO `sys_roles_menus` VALUES (64, 1);
INSERT INTO `sys_roles_menus` VALUES (65, 1);
INSERT INTO `sys_roles_menus` VALUES (66, 1);
INSERT INTO `sys_roles_menus` VALUES (73, 1);
INSERT INTO `sys_roles_menus` VALUES (74, 1);
INSERT INTO `sys_roles_menus` VALUES (75, 1);
INSERT INTO `sys_roles_menus` VALUES (77, 1);
INSERT INTO `sys_roles_menus` VALUES (78, 1);
INSERT INTO `sys_roles_menus` VALUES (79, 1);
INSERT INTO `sys_roles_menus` VALUES (80, 1);
INSERT INTO `sys_roles_menus` VALUES (82, 1);
INSERT INTO `sys_roles_menus` VALUES (83, 1);
INSERT INTO `sys_roles_menus` VALUES (90, 1);
INSERT INTO `sys_roles_menus` VALUES (92, 1);
INSERT INTO `sys_roles_menus` VALUES (93, 1);
INSERT INTO `sys_roles_menus` VALUES (94, 1);
INSERT INTO `sys_roles_menus` VALUES (97, 1);
INSERT INTO `sys_roles_menus` VALUES (98, 1);
INSERT INTO `sys_roles_menus` VALUES (102, 1);
INSERT INTO `sys_roles_menus` VALUES (103, 1);
INSERT INTO `sys_roles_menus` VALUES (104, 1);
INSERT INTO `sys_roles_menus` VALUES (105, 1);
INSERT INTO `sys_roles_menus` VALUES (106, 1);
INSERT INTO `sys_roles_menus` VALUES (107, 1);
INSERT INTO `sys_roles_menus` VALUES (108, 1);
INSERT INTO `sys_roles_menus` VALUES (109, 1);
INSERT INTO `sys_roles_menus` VALUES (110, 1);
INSERT INTO `sys_roles_menus` VALUES (111, 1);
INSERT INTO `sys_roles_menus` VALUES (112, 1);
INSERT INTO `sys_roles_menus` VALUES (113, 1);
INSERT INTO `sys_roles_menus` VALUES (114, 1);
INSERT INTO `sys_roles_menus` VALUES (116, 1);
INSERT INTO `sys_roles_menus` VALUES (118, 1);
INSERT INTO `sys_roles_menus` VALUES (119, 1);
INSERT INTO `sys_roles_menus` VALUES (120, 1);
INSERT INTO `sys_roles_menus` VALUES (122, 1);
INSERT INTO `sys_roles_menus` VALUES (123, 1);
INSERT INTO `sys_roles_menus` VALUES (124, 1);
INSERT INTO `sys_roles_menus` VALUES (125, 1);
INSERT INTO `sys_roles_menus` VALUES (126, 1);
INSERT INTO `sys_roles_menus` VALUES (127, 1);
INSERT INTO `sys_roles_menus` VALUES (128, 1);
INSERT INTO `sys_roles_menus` VALUES (129, 1);
INSERT INTO `sys_roles_menus` VALUES (130, 1);
INSERT INTO `sys_roles_menus` VALUES (131, 1);
INSERT INTO `sys_roles_menus` VALUES (132, 1);
INSERT INTO `sys_roles_menus` VALUES (133, 1);
INSERT INTO `sys_roles_menus` VALUES (134, 1);
INSERT INTO `sys_roles_menus` VALUES (135, 1);
INSERT INTO `sys_roles_menus` VALUES (136, 1);
INSERT INTO `sys_roles_menus` VALUES (137, 1);
INSERT INTO `sys_roles_menus` VALUES (138, 1);
INSERT INTO `sys_roles_menus` VALUES (139, 1);
INSERT INTO `sys_roles_menus` VALUES (140, 1);
INSERT INTO `sys_roles_menus` VALUES (141, 1);
INSERT INTO `sys_roles_menus` VALUES (142, 1);
INSERT INTO `sys_roles_menus` VALUES (143, 1);
INSERT INTO `sys_roles_menus` VALUES (144, 1);
INSERT INTO `sys_roles_menus` VALUES (2, 2);
INSERT INTO `sys_roles_menus` VALUES (44, 2);
INSERT INTO `sys_roles_menus` VALUES (119, 2);
INSERT INTO `sys_roles_menus` VALUES (122, 2);
INSERT INTO `sys_roles_menus` VALUES (123, 2);
INSERT INTO `sys_roles_menus` VALUES (125, 2);
INSERT INTO `sys_roles_menus` VALUES (129, 2);
INSERT INTO `sys_roles_menus` VALUES (130, 2);
INSERT INTO `sys_roles_menus` VALUES (131, 2);
INSERT INTO `sys_roles_menus` VALUES (135, 2);
INSERT INTO `sys_roles_menus` VALUES (139, 2);
INSERT INTO `sys_roles_menus` VALUES (142, 2);
INSERT INTO `sys_roles_menus` VALUES (145, 2);
INSERT INTO `sys_roles_menus` VALUES (146, 2);
INSERT INTO `sys_roles_menus` VALUES (1, 3);
INSERT INTO `sys_roles_menus` VALUES (2, 3);
INSERT INTO `sys_roles_menus` VALUES (3, 3);
INSERT INTO `sys_roles_menus` VALUES (6, 3);
INSERT INTO `sys_roles_menus` VALUES (7, 3);
INSERT INTO `sys_roles_menus` VALUES (9, 3);
INSERT INTO `sys_roles_menus` VALUES (32, 3);
INSERT INTO `sys_roles_menus` VALUES (37, 3);
INSERT INTO `sys_roles_menus` VALUES (39, 3);
INSERT INTO `sys_roles_menus` VALUES (41, 3);
INSERT INTO `sys_roles_menus` VALUES (44, 3);
INSERT INTO `sys_roles_menus` VALUES (45, 3);
INSERT INTO `sys_roles_menus` VALUES (46, 3);
INSERT INTO `sys_roles_menus` VALUES (48, 3);
INSERT INTO `sys_roles_menus` VALUES (49, 3);
INSERT INTO `sys_roles_menus` VALUES (50, 3);
INSERT INTO `sys_roles_menus` VALUES (60, 3);
INSERT INTO `sys_roles_menus` VALUES (61, 3);
INSERT INTO `sys_roles_menus` VALUES (62, 3);
INSERT INTO `sys_roles_menus` VALUES (64, 3);
INSERT INTO `sys_roles_menus` VALUES (65, 3);
INSERT INTO `sys_roles_menus` VALUES (66, 3);
INSERT INTO `sys_roles_menus` VALUES (80, 3);
INSERT INTO `sys_roles_menus` VALUES (118, 3);
INSERT INTO `sys_roles_menus` VALUES (119, 3);
INSERT INTO `sys_roles_menus` VALUES (122, 3);
INSERT INTO `sys_roles_menus` VALUES (123, 3);
INSERT INTO `sys_roles_menus` VALUES (124, 3);
INSERT INTO `sys_roles_menus` VALUES (125, 3);
INSERT INTO `sys_roles_menus` VALUES (126, 3);
INSERT INTO `sys_roles_menus` VALUES (127, 3);
INSERT INTO `sys_roles_menus` VALUES (128, 3);
INSERT INTO `sys_roles_menus` VALUES (129, 3);
INSERT INTO `sys_roles_menus` VALUES (130, 3);
INSERT INTO `sys_roles_menus` VALUES (131, 3);
INSERT INTO `sys_roles_menus` VALUES (132, 3);
INSERT INTO `sys_roles_menus` VALUES (133, 3);
INSERT INTO `sys_roles_menus` VALUES (134, 3);
INSERT INTO `sys_roles_menus` VALUES (135, 3);
INSERT INTO `sys_roles_menus` VALUES (136, 3);
INSERT INTO `sys_roles_menus` VALUES (137, 3);
INSERT INTO `sys_roles_menus` VALUES (138, 3);
INSERT INTO `sys_roles_menus` VALUES (139, 3);
INSERT INTO `sys_roles_menus` VALUES (140, 3);
INSERT INTO `sys_roles_menus` VALUES (141, 3);
INSERT INTO `sys_roles_menus` VALUES (142, 3);
INSERT INTO `sys_roles_menus` VALUES (143, 3);
INSERT INTO `sys_roles_menus` VALUES (144, 3);
INSERT INTO `sys_roles_menus` VALUES (145, 3);
INSERT INTO `sys_roles_menus` VALUES (146, 3);
INSERT INTO `sys_roles_menus` VALUES (147, 3);
INSERT INTO `sys_roles_menus` VALUES (148, 3);
INSERT INTO `sys_roles_menus` VALUES (149, 3);
INSERT INTO `sys_roles_menus` VALUES (150, 3);
INSERT INTO `sys_roles_menus` VALUES (151, 3);
INSERT INTO `sys_roles_menus` VALUES (152, 3);
INSERT INTO `sys_roles_menus` VALUES (2, 4);
INSERT INTO `sys_roles_menus` VALUES (3, 4);
INSERT INTO `sys_roles_menus` VALUES (44, 4);
INSERT INTO `sys_roles_menus` VALUES (45, 4);
INSERT INTO `sys_roles_menus` VALUES (118, 4);
INSERT INTO `sys_roles_menus` VALUES (119, 4);
INSERT INTO `sys_roles_menus` VALUES (122, 4);
INSERT INTO `sys_roles_menus` VALUES (123, 4);
INSERT INTO `sys_roles_menus` VALUES (124, 4);
INSERT INTO `sys_roles_menus` VALUES (125, 4);
INSERT INTO `sys_roles_menus` VALUES (126, 4);
INSERT INTO `sys_roles_menus` VALUES (127, 4);
INSERT INTO `sys_roles_menus` VALUES (128, 4);
INSERT INTO `sys_roles_menus` VALUES (129, 4);
INSERT INTO `sys_roles_menus` VALUES (130, 4);
INSERT INTO `sys_roles_menus` VALUES (131, 4);
INSERT INTO `sys_roles_menus` VALUES (132, 4);
INSERT INTO `sys_roles_menus` VALUES (133, 4);
INSERT INTO `sys_roles_menus` VALUES (134, 4);
INSERT INTO `sys_roles_menus` VALUES (135, 4);
INSERT INTO `sys_roles_menus` VALUES (136, 4);
INSERT INTO `sys_roles_menus` VALUES (137, 4);
INSERT INTO `sys_roles_menus` VALUES (138, 4);
INSERT INTO `sys_roles_menus` VALUES (139, 4);
INSERT INTO `sys_roles_menus` VALUES (140, 4);
INSERT INTO `sys_roles_menus` VALUES (141, 4);
INSERT INTO `sys_roles_menus` VALUES (142, 4);
INSERT INTO `sys_roles_menus` VALUES (143, 4);
INSERT INTO `sys_roles_menus` VALUES (144, 4);
INSERT INTO `sys_roles_menus` VALUES (145, 4);
INSERT INTO `sys_roles_menus` VALUES (146, 4);
INSERT INTO `sys_roles_menus` VALUES (147, 4);
INSERT INTO `sys_roles_menus` VALUES (148, 4);
INSERT INTO `sys_roles_menus` VALUES (149, 4);
INSERT INTO `sys_roles_menus` VALUES (150, 4);
INSERT INTO `sys_roles_menus` VALUES (151, 4);
INSERT INTO `sys_roles_menus` VALUES (152, 4);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dept_id` bigint NULL DEFAULT NULL COMMENT '部门名称',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `gender` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `avatar_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像地址',
  `avatar_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像真实路径',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `is_admin` bit(1) NULL DEFAULT b'0' COMMENT '是否为admin账号',
  `enabled` bigint NULL DEFAULT NULL COMMENT '状态：1启用、0禁用',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `pwd_reset_time` datetime NULL DEFAULT NULL COMMENT '修改密码的时间',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `UK_kpubos9gc2cvtkb0thktkbkes`(`email`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `uniq_username`(`username`) USING BTREE,
  UNIQUE INDEX `uniq_email`(`email`) USING BTREE,
  INDEX `FK5rwmryny6jthaaxkogownknqp`(`dept_id`) USING BTREE,
  INDEX `FKpq2dhypk2qgt68nauh2by22jb`(`avatar_name`) USING BTREE,
  INDEX `inx_enabled`(`enabled`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 2, 'admin', '管理员', '男', '18888888888', '201507802@qq.com', 'avatar-20200806032259161.png', '/Users/jie/Documents/work/me/admin/eladmin/~/avatar/avatar-20200806032259161.png', '$2a$10$Egp1/gvFlt7zhlXVfEFw4OfWQCGPw0ClmMcc6FjTnvXNRVf9zdMRa', b'1', 1, NULL, 'admin', '2020-05-03 16:38:31', '2018-08-23 09:11:56', '2020-09-05 10:43:31');
INSERT INTO `sys_user` VALUES (2, 2, 'test', '测试', '男', '19999999999', '231@qq.com', NULL, NULL, '$2a$10$4XcyudOYTSz6fue6KFNMHeUQnCX5jbBQypLEnGk1PmekXt5c95JcK', b'0', 1, 'admin', 'admin', NULL, '2020-05-05 11:15:49', '2020-09-05 10:43:38');
INSERT INTO `sys_user` VALUES (3, 5, '999', '大老板', '男', '18188888888', '18188888888@qq.com', NULL, NULL, '$2a$10$nns8tSfMfXvCcKWeWS1ukOj99f1yhH3t.kv58/gtvBu3oWRxFQsfy', b'0', 1, 'admin', 'admin', NULL, '2021-04-30 09:26:44', '2021-04-30 09:26:44');

-- ----------------------------
-- Table structure for sys_users_jobs
-- ----------------------------
DROP TABLE IF EXISTS `sys_users_jobs`;
CREATE TABLE `sys_users_jobs`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `job_id` bigint NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`, `job_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_users_jobs
-- ----------------------------
INSERT INTO `sys_users_jobs` VALUES (1, 11);
INSERT INTO `sys_users_jobs` VALUES (2, 12);
INSERT INTO `sys_users_jobs` VALUES (3, 14);

-- ----------------------------
-- Table structure for sys_users_roles
-- ----------------------------
DROP TABLE IF EXISTS `sys_users_roles`;
CREATE TABLE `sys_users_roles`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  INDEX `FKq4eq273l04bpu4efj0jd0jb98`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色关联' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of sys_users_roles
-- ----------------------------
INSERT INTO `sys_users_roles` VALUES (1, 1);
INSERT INTO `sys_users_roles` VALUES (2, 2);
INSERT INTO `sys_users_roles` VALUES (3, 3);

-- ----------------------------
-- Table structure for tool_alipay_config
-- ----------------------------
DROP TABLE IF EXISTS `tool_alipay_config`;
CREATE TABLE `tool_alipay_config`  (
  `config_id` bigint NOT NULL COMMENT 'ID',
  `app_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '应用ID',
  `charset` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '编码',
  `format` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型 固定格式json',
  `gateway_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '网关地址',
  `notify_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '异步回调',
  `private_key` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '私钥',
  `public_key` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '公钥',
  `return_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回调地址',
  `sign_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '签名方式',
  `sys_service_provider_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商户号',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '支付宝配置类' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tool_alipay_config
-- ----------------------------
INSERT INTO `tool_alipay_config` VALUES (1, '2016091700532697', 'utf-8', 'JSON', 'https://openapi.alipaydev.com/gateway.do', 'http://api.auauz.net/api/aliPay/notify', 'MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC5js8sInU10AJ0cAQ8UMMyXrQ+oHZEkVt5lBwsStmTJ7YikVYgbskx1YYEXTojRsWCb+SH/kDmDU4pK/u91SJ4KFCRMF2411piYuXU/jF96zKrADznYh/zAraqT6hvAIVtQAlMHN53nx16rLzZ/8jDEkaSwT7+HvHiS+7sxSojnu/3oV7BtgISoUNstmSe8WpWHOaWv19xyS+Mce9MY4BfseFhzTICUymUQdd/8hXA28/H6osUfAgsnxAKv7Wil3aJSgaJczWuflYOve0dJ3InZkhw5Cvr0atwpk8YKBQjy5CdkoHqvkOcIB+cYHXJKzOE5tqU7inSwVbHzOLQ3XbnAgMBAAECggEAVJp5eT0Ixg1eYSqFs9568WdetUNCSUchNxDBu6wxAbhUgfRUGZuJnnAll63OCTGGck+EGkFh48JjRcBpGoeoHLL88QXlZZbC/iLrea6gcDIhuvfzzOffe1RcZtDFEj9hlotg8dQj1tS0gy9pN9g4+EBH7zeu+fyv+qb2e/v1l6FkISXUjpkD7RLQr3ykjiiEw9BpeKb7j5s7Kdx1NNIzhkcQKNqlk8JrTGDNInbDM6inZfwwIO2R1DHinwdfKWkvOTODTYa2MoAvVMFT9Bec9FbLpoWp7ogv1JMV9svgrcF9XLzANZ/OQvkbe9TV9GWYvIbxN6qwQioKCWO4GPnCAQKBgQDgW5MgfhX8yjXqoaUy/d1VjI8dHeIyw8d+OBAYwaxRSlCfyQ+tieWcR2HdTzPca0T0GkWcKZm0ei5xRURgxt4DUDLXNh26HG0qObbtLJdu/AuBUuCqgOiLqJ2f1uIbrz6OZUHns+bT/jGW2Ws8+C13zTCZkZt9CaQsrp3QOGDx5wKBgQDTul39hp3ZPwGNFeZdkGoUoViOSd5Lhowd5wYMGAEXWRLlU8z+smT5v0POz9JnIbCRchIY2FAPKRdVTICzmPk2EPJFxYTcwaNbVqL6lN7J2IlXXMiit5QbiLauo55w7plwV6LQmKm9KV7JsZs5XwqF7CEovI7GevFzyD3w+uizAQKBgC3LY1eRhOlpWOIAhpjG6qOoohmeXOphvdmMlfSHq6WYFqbWwmV4rS5d/6LNpNdL6fItXqIGd8I34jzql49taCmi+A2nlR/E559j0mvM20gjGDIYeZUz5MOE8k+K6/IcrhcgofgqZ2ZED1ksHdB/E8DNWCswZl16V1FrfvjeWSNnAoGAMrBplCrIW5xz+J0Hm9rZKrs+AkK5D4fUv8vxbK/KgxZ2KaUYbNm0xv39c+PZUYuFRCz1HDGdaSPDTE6WeWjkMQd5mS6ikl9hhpqFRkyh0d0fdGToO9yLftQKOGE/q3XUEktI1XvXF0xyPwNgUCnq0QkpHyGVZPtGFxwXiDvpvgECgYA5PoB+nY8iDiRaJNko9w0hL4AeKogwf+4TbCw+KWVEn6jhuJa4LFTdSqp89PktQaoVpwv92el/AhYjWOl/jVCm122f9b7GyoelbjMNolToDwe5pF5RnSpEuDdLy9MfE8LnE3PlbE7E5BipQ3UjSebkgNboLHH/lNZA5qvEtvbfvQ==', 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAut9evKRuHJ/2QNfDlLwvN/S8l9hRAgPbb0u61bm4AtzaTGsLeMtScetxTWJnVvAVpMS9luhEJjt+Sbk5TNLArsgzzwARgaTKOLMT1TvWAK5EbHyI+eSrc3s7Awe1VYGwcubRFWDm16eQLv0k7iqiw+4mweHSz/wWyvBJVgwLoQ02btVtAQErCfSJCOmt0Q/oJQjj08YNRV4EKzB19+f5A+HQVAKy72dSybTzAK+3FPtTtNen/+b5wGeat7c32dhYHnGorPkPeXLtsqqUTp1su5fMfd4lElNdZaoCI7osZxWWUo17vBCZnyeXc9fk0qwD9mK6yRAxNbrY72Xx5VqIqwIDAQAB', 'http://api.auauz.net/api/aliPay/return', 'RSA2', '2088102176044281');

-- ----------------------------
-- Table structure for tool_email_config
-- ----------------------------
DROP TABLE IF EXISTS `tool_email_config`;
CREATE TABLE `tool_email_config`  (
  `config_id` bigint NOT NULL COMMENT 'ID',
  `from_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '收件人',
  `host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮件服务器SMTP地址',
  `pass` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `port` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '端口',
  `user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发件者用户名',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '邮箱配置' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tool_email_config
-- ----------------------------

-- ----------------------------
-- Table structure for tool_local_storage
-- ----------------------------
DROP TABLE IF EXISTS `tool_local_storage`;
CREATE TABLE `tool_local_storage`  (
  `storage_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `real_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件真实的名称',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名',
  `suffix` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '后缀',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '路径',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类型',
  `size` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '大小',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `update_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '更新者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建日期',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`storage_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '本地存储' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tool_local_storage
-- ----------------------------

-- ----------------------------
-- Table structure for tool_qiniu_config
-- ----------------------------
DROP TABLE IF EXISTS `tool_qiniu_config`;
CREATE TABLE `tool_qiniu_config`  (
  `config_id` bigint NOT NULL COMMENT 'ID',
  `access_key` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'accessKey',
  `bucket` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Bucket 识别符',
  `host` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '外链域名',
  `secret_key` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'secretKey',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '空间类型',
  `zone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '机房',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '七牛云配置' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tool_qiniu_config
-- ----------------------------

-- ----------------------------
-- Table structure for tool_qiniu_content
-- ----------------------------
DROP TABLE IF EXISTS `tool_qiniu_content`;
CREATE TABLE `tool_qiniu_content`  (
  `content_id` bigint NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `bucket` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Bucket 识别符',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件名称',
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件大小',
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件类型：私有或公开',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件url',
  `suffix` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '文件后缀',
  `update_time` datetime NULL DEFAULT NULL COMMENT '上传或同步的时间',
  PRIMARY KEY (`content_id`) USING BTREE,
  UNIQUE INDEX `uniq_name`(`name`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '七牛云文件存储' ROW_FORMAT = COMPACT;

-- ----------------------------
-- Records of tool_qiniu_content
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
