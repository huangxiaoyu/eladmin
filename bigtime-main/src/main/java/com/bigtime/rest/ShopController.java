/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.rest;

import me.zhengjie.annotation.Log;
import com.bigtime.domain.Shop;
import com.bigtime.service.ShopService;
import com.bigtime.service.dto.ShopQueryCriteria;
import org.springframework.data.domain.Pageable;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @website https://el-admin.vip
* @author huangxiaoyu
* @date 2021-04-29
**/
@RestController
@RequiredArgsConstructor
@Api(tags = "业务:店铺管理")
@RequestMapping("/api/shop")
public class ShopController {

    private final ShopService shopService;

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('shop:list')")
    public void download(HttpServletResponse response, ShopQueryCriteria criteria) throws IOException {
        shopService.download(shopService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询shop")
    @ApiOperation("查询shop")
    @PreAuthorize("@el.check('shop:list')")
    public ResponseEntity<Object> query(ShopQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(shopService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增shop")
    @ApiOperation("新增shop")
    @PreAuthorize("@el.check('shop:add')")
    public ResponseEntity<Object> create(@Validated @RequestBody Shop resources){
        return new ResponseEntity<>(shopService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改shop")
    @ApiOperation("修改shop")
    @PreAuthorize("@el.check('shop:edit')")
    public ResponseEntity<Object> update(@Validated @RequestBody Shop resources){
        shopService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除shop")
    @ApiOperation("删除shop")
    @PreAuthorize("@el.check('shop:del')")
    @DeleteMapping
    public ResponseEntity<Object> delete(@RequestBody Long[] ids) {
        shopService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}