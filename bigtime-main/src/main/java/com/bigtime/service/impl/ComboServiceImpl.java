/*
*  Copyright 2019-2020 Zheng Jie
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*  http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/
package com.bigtime.service.impl;

import com.bigtime.domain.Combo;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import lombok.RequiredArgsConstructor;
import com.bigtime.repository.ComboRepository;
import com.bigtime.service.ComboService;
import com.bigtime.service.dto.ComboDto;
import com.bigtime.service.dto.ComboQueryCriteria;
import com.bigtime.service.mapstruct.ComboMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @website https://el-admin.vip
* @description 服务实现
* @author huangxiaoyu
* @date 2021-04-29
**/
@Service
@RequiredArgsConstructor
public class ComboServiceImpl implements ComboService {

    private final ComboRepository comboRepository;
    private final ComboMapper comboMapper;

    @Override
    public Map<String,Object> queryAll(ComboQueryCriteria criteria, Pageable pageable){
        Page<Combo> page = comboRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(comboMapper::toDto));
    }

    @Override
    public List<ComboDto> queryAll(ComboQueryCriteria criteria){
        return comboMapper.toDto(comboRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    @Transactional
    public ComboDto findById(Long id) {
        Combo combo = comboRepository.findById(id).orElseGet(Combo::new);
        ValidationUtil.isNull(combo.getId(),"Combo","id",id);
        return comboMapper.toDto(combo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ComboDto create(Combo resources) {
        return comboMapper.toDto(comboRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(Combo resources) {
        Combo combo = comboRepository.findById(resources.getId()).orElseGet(Combo::new);
        ValidationUtil.isNull( combo.getId(),"Combo","id",resources.getId());
        combo.copy(resources);
        comboRepository.save(combo);
    }

    @Override
    public void deleteAll(Long[] ids) {
        for (Long id : ids) {
            comboRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<ComboDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (ComboDto combo : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("名称", combo.getName());
            map.put("价格", combo.getPrice());
            map.put("时长", combo.getTime());
            map.put("赠送时长", combo.getFreeTime());
            map.put("提成", combo.getPushMoney());
            map.put("店铺id", combo.getShopId());
            map.put("是否启用", combo.getEnabled());
            map.put("谁修改的", combo.getUdpateBy());
            map.put("修改时间", combo.getUdpateDate());
            map.put("创建时间", combo.getCreateDate());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}